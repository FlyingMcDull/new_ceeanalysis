    <%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>高考情况 ---CEEAnalysis</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/manage.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet">
    <link href="css/input.css" rel="stylesheet">
    <style type="text/css">
        #header{
            width: 100%;
            height: 134px;
            /*background-color: #f5f5f5;*/
            background: url("images/footer.png") center center;
            position: relative;
        }

        #container #mainContent #content
        {
            float: left;
            width: 80%;
        }
        
        .content_frame {
            border-bottom: solid 2px #337ab7;
            padding: 20px;
            margin-left: 5%;
            margin-right: 10%;
        
        }
        

        #container #mainContent {
            width: 1000px;
            height: 700px;
            margin-top: 50px;
            margin-left: auto;
            margin-right: auto;
        }
            
        #container #mainContent #sidebar .radius {
            border-width: 1px;
            border-style: solid;
            border-color: rgb(141, 143, 139);
            border-radius: 3px;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
            width: 180px;
            line-height: 75px;
            font-size: 18px;
            text-align: center;
            background-color: #337ab7;
            color: white;
            margin-top: 300px;
        }
    
        #container #mainContent #content #manageform #content2 .content2_p {
            margin-top: 15px;
            text-align: center;
            font-size: 16px;
        }
        #container #mainContent #content #manageform #content3 {
            text-align: center;
            padding-top: 30px;
            border-top: solid 2px #337ab7;
            margin: 20px;
        }
    
        #container #mainContent #content #manageform #content1 {
            font-size: 16px;
            display:block;
            margin: 20px;
            padding-left: 20px;
        }

        #container #buttom {
            background-color: #FFF;
            width: 100%;
            height: 70px;
            margin-top: 50px;
        }

        #container #mainContent #content #manageform #content3 .submit_btn {
            line-height: 40px;
            height: 40px;
            padding-left: 5px;
            padding-right: 5px;
            color:#ffffff;
            background-color:#ededed;
            font-size: 14px;
            font-weight:bold;
            font-family:Arial;
            background:-webkit-gradient(linear, left top, left bottom, color-start(0.05, #79bbff), color-stop(1, #378de5));
            background:-moz-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background:-o-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background:-ms-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background:linear-gradient(to bottom, #79bbff 5%, #378de5 100%);
            background:-webkit-linear-gradient(top, #79bbff 5%, #378de5 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5',GradientType=0);
            border: 1px solid #337ab7;
            -webkit-border-top-left-radius: 6px;
            -moz-border-radius-topleft: 6px;
            border-top-left-radius: 6px;
            -webkit-border-top-right-radius: 6px;
            -moz-border-radius-topright: 6px;
            border-top-right-radius: 6px;
            -webkit-border-bottom-left-radius: 6px;
            -moz-border-radius-bottomleft: 6px;
            border-bottom-left-radius: 6px;
            -webkit-border-bottom-right-radius: 6px;
            -moz-border-radius-bottomright: 6px;
            border-bottom-right-radius: 6px;
            -moz-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            -webkit-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            text-align: center;
            display: inline-block;
            text-decoration: none;
        }

        #container #mainContent #content #manageform #content2 .content_title {
            color: #509898;
            background-color: #F5F5F5;
            margin-top: -60px;
            position: absolute;
            line-height: 40px;
            font-size: 18px;
            width: 100px;
            text-align: center;
            margin-left:-50px;
        }

        #container #mainContent #content #manageform #content2 {
            text-align: center;
            border-top: solid 2px #337ab7;
            margin: 20px;
            padding-top: 40px;

        }

        #container #mainContent #content #manageform #content2 .content_ul {
            margin: 0px;
            padding: 0px;
        }

        #container #mainContent #content #manageform #content3 .submit_btn:hover {
            background-color:#f5f5f5;
            background:-webkit-gradient(linear, left top, left bottom, color-start(0.05, #378de5), color-stop(1, #79bbff));
            background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
            background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
        }
 
        #container #mainContent #sidebar {
            float: left;
            width: 18%;
            height: 100%;
            border-right: solid 2px #337ab7;
        }


        #container #mainContent #content #manageform #content2 .content_ul li {
            margin-top: 30px;
            margin-bottom: 30px;
            font-size: 14px;
            line-height: 20px;
            text-align: left;
        }

        #container #mainContent #content #manageform #content2 .content_ul li span {
            margin-right: 30px;
            margin-left: 30px;
        }
    </style>
    <link href="css/footer.css" rel="stylesheet">
    <script src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript">
    </script>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="platform-header">
            <div id="platform-user">
               <div class="top-show">
                    <ul>
                        <li><a href="javascript:void(0);" data-src="" >收藏此页</a></li>
                    </ul>
                </div>
                <div class="top-show" style="float: right;">
                    <ul>
                        <li><a  href="login.html" data-click="">退出登录</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <div id="logo-header">
            <div id="logo-left">
                <img src="images/logo.png" alt="高考系统">
            </div>
            <div id="logo-right"></div>
        </div>
    </div>


    <nav  class="main-navigation">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 hdcolor">
                    <div class="navbar-header">
                        <span class="nav-toggle-button collapse" data-target="#main-menu">
                        <span class="sr-only">高考志愿填报分析助手导航</span>
                        <i class="fa fa-bars"></i>
                        </span>
                    </div>
                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="menu">
                            <li class="nav-current" role="presentation"><a class="menu-name" href="home_page.jsp">首页</a></li>
                            <li role="presentation"><a class="menu-name"   href="manage.jsp">用户管理</a></li>
                            <li role="presentation"><a class="menu-name selected" href="estimated.jsp">高考查询</a></li>
                            <li role="presentation"><a class="menu-name" href="analysis.jsp">专业评测</a></li>
                            <li role="presentation"><a class="menu-name" href="rank_page.html">院校信息</a></li>
                            <li role="presentation"><a class="menu-name" href="Recommendation.jsp">填报推荐</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="mainContent">
    <div id="sidebar">
       <p class="radius">下载试卷和估分</p>
    </div>
    <div id="content">
  <!--      <form id="manageform" class="manageform"> -->
       <div id="manageform" class="manageform">
      <div id="content1">
      <%
                    String p;
                    int proId = Integer.parseInt(request.getSession().getAttribute("provinceid").toString());
                    switch (proId)
                    {
                    	case 0:
                    		p = "北京";
                    		break;
                       	case 1 :
                       		p = "上海";
                         	break;
                       	case 2 :
                       		p = "湖北";
                         	break;
                       	case 3 :
                       		p = "河南";
                       		break;
                       	default :
                       		p = "湖北";
                    }
                    String type = "wen";
				    if(type==(request.getSession().getAttribute("usertype").toString()))
				    	type = "文科";
				    else
				    	type = "理科";
                    %>
            <p>你的个人资料显示为：<%= p %>省<%= type %></p>
       </div>
       
        <div id="content2" >
           <span class="content_title">试卷连接</span>
           
              <ul class="content_ul">
                <li><span class="down">语文</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷下载</html:link></li>
                <li><span class="down">数学</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷下载</html:link></li>
                <li><span class="down">外语</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷下载</html:link></li>
                <li><span class="down">综合</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷下载</html:link></li>
              </ul>
             
            </div>
            
             <div id="content2" >
           <span class="content_title">答案链接</span>
               <ul class="content_ul">
                  <li><span class="down" >语文</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷答案下载</html:link></li>
                <li><span class="down">数学</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷答案下载</html:link></li>
                <li><span class="down">外语</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷答案下载</html:link></li>
                <li><span class="down">综合</span> <html:link action = "/downloadFile?fileName=test.doc">本省高考试卷答案下载</html:link></li>
              </ul>
             
            </div>
             <div id="content3" >
                <button  class="button3" onclick="window.location.href='score.html'">进入分数线预测</button>
            </div>
            </div>
<!--        </form> -->
    </div>
    
    </div>

    <div id="buttom">
        <div class="footer-address">
            <div class="footer-address-all">
                <div class="footer-address-left">
                    <div class="mm">地址：华中科技大学</div>
				   <div class="mm">电话查号台：010-62793001</div>
				   <div class="mm">华中科技大学•五只麦兜项目组荣誉出品</div>
                </div>
                <div class="footer-address-right">
                    <div class="mm">京公网安备 110402430053 号</div>
                </div>
            </div>
        </div>
    </div>

</div>


</body>
</html>