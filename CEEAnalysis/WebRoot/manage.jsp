<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>用户管理 ---CEEAnalysis</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/manage.css" rel="stylesheet">
    <link href="css/tag.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">

<style type="text/css">
    .radius {
        border-width: 2px;
        border-style: solid;
        border-color: gainsboro;
        border-radius: 3px;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
        border-bottom-left-radius: 3px;
        border-bottom-right-radius: 3px;
        width: 180px;
        line-height: 70px;
        font-size: 25px;
        text-align: center;
        background-color: #E8E8E8;
        color: darkgrey;
    }
    #header{
        width: 100%;
        height: 134px;
        /*background-color: #f5f5f5;*/
        background: url("images/footer.png") center center;
        position: relative;
    }
</style>
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/manage.js"></script>
    <script src="js/select.js"></script>
    <script src="js/Check.js"></script>


</head>
<body>
<div id="container">
    <div id="header">
        <div id="platform-header">
            <div id="platform-user">
                 <div class="top-show">
                    <ul>
                        <li><a href="javascript:void(0);" data-src="" >收藏此页</a></li>
                    </ul>
                </div>
                <div class="top-show" style="float: right;">
                    <a href="login.html">退出登录</a>

                </div>
            </div>
        </div>
        <div id="logo-header">
            <div id="logo-left">
                <img src="images/logo.png" alt="高考系统">
            </div>
            <div id="logo-right"></div>
        </div>
    </div>


    <nav  class="main-navigation">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 hdcolor">
                    <div class="navbar-header">
                        <span class="nav-toggle-button collapse" data-target="#main-menu">
                        <span class="sr-only">高考志愿填报分析助手导航</span>
                        <i class="fa fa-bars"></i>
                        </span>
                    </div>
                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="menu">
                            <li id="home_page" class="nav-current" role="presentation"><a class="menu-name" href="home_page.jsp">首页</a></li>
                            <li id="manage" role="presentation" ><a class="menu-name  selected"  href="manage.jsp">用户管理</a></li>
                            <li id="estimated" role="presentation"><a class="menu-name" href=estimated.jsp>高考查询</a></li>
                            <li id="analysis" role="presentation"><a class="menu-name" href="analysis.jsp">专业评测</a></li>
                            <li id="rank_page" role="presentation"><a class="menu-name" href="rank_page.html">院校信息</a></li>
                            <li id="recommendation" role="presentation"><a class="menu-name" href="Recommendation.jsp">填报推荐</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>


    <div id="mainContent" class="mainContent">
        <!--导航栏-->
        <div id="sidebar" class="sidebar" >
            <div class="sideframe"><div class="radius"  style="margin-top: 100px;margin-right: auto;margin-left: auto">基本资料</div></div>
            <div class="sideframe"><div class="radius"  style="margin-top: 270px;margin-right: auto;margin-left: auto">修改密码</div></div>
            <div class="sideframe"><div class="radius"  style="margin-top: 240px;margin-right: auto;margin-left: auto">绑定邮箱</div></div>
            <!--<div class="sideframe"><div class="radius"  style="margin-top: 280px">资料补充</div></div>-->
        </div>
        <div id="content" class="content">

            <div id="content1" class="frame bdbottom">
                <div id="stable1" class="stable1">
                <table>
                    <tr>
                        <td>
                            <span  class="font1">用户名:</span>
                            &nbsp;&nbsp;&nbsp;<span  class="font2"><%= request.getSession().getAttribute("username") %></span>
                        </td>
                    </tr>
                    <%
                    String p;
                    int proId = Integer.parseInt(request.getSession().getAttribute("provinceid").toString());
                    switch (proId)
                    {
                    	case 0:
                    		p = "北京";
                    		break;
                       	case 1 :
                       		p = "上海";
                         	break;
                       	case 2 :
                       		p = "湖北";
                         	break;
                       	case 3 :
                       		p = "河南";
                       		break;
                       	default :
                       		p = "湖北";
               	  
                    }
                    %>
                    <tr>
                        <td>
                            <span class="font1">生源地:</span>
                            &nbsp;&nbsp;&nbsp;<span  class="font2"><%= p %></span>
                        </td>
                    </tr>
                    <%
                    	String type = "wen";
					    if(type==(request.getSession().getAttribute("usertype").toString()))
					    	type = "文科";
					    else
					    	type = "理科";
                    %>
                    <tr>
                        <td>
                            <span  class="font1">考生类别:</span>
                            &nbsp;&nbsp;&nbsp;<span class="font2"><%= type %></span>
                        </td>
                    </tr>
                    <!--<tr>-->
                        <!--<td>-->
                            <!--<button id="a1" type="button"   class="button">编辑个人资料</button>-->
                        <!--</td>-->
                    <!--</tr>-->
                </table>
                </div>
            </div>
            <div id="content2" class="frame bdbottom">
                <div id="stable2" class="resetpw">
                <table>
                    <tr>
                        <td><p class="inline"></p></td>
                    </tr>
                    <tr>
                        <td>
                            <button id="a2" type="button"  class="button">修改登录密码</button>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <div id="content3" class="frame" >
                <div id="stable3" class="email">
                    <table>
                        <tr>
                            <span  class="font1">邮箱地址:</span>
                            &nbsp;&nbsp;&nbsp;<span class="font2"><%= request.getSession().getAttribute("useremail") %></span>
                        </tr>
                        <tr>
                            <td>
                                <button id="a3" type="button"  class="button">更换绑定邮箱</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

                <!--<div id="content4" class="frame " >-->
                    <!--<div id="stable4" class="extra">-->
                        <!--<table>-->
                            <!--<tr>-->
                                <!--<td>-->
                                    <!--<span  class="font1">兴趣专业:</span>-->
                                    <!--&nbsp;&nbsp;&nbsp;<span  class="font2">计算机网络&lt;!&ndash;&ndash;&gt;</span>-->
                                <!--</td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td>-->
                                    <!--<span class="font1">特长学科:</span>-->
                                    <!--&nbsp;&nbsp;&nbsp;<span  class="font2">数学&lt;!&ndash;&ndash;&gt;</span>-->
                                <!--</td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td>-->
                                    <!--<span  class="font1">籍贯:</span>-->
                                    <!--&nbsp;&nbsp;&nbsp;<span class="font2">湖北武汉&lt;!&ndash;&ndash;&gt;</span>-->
                                <!--</td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td>-->
                                    <!--<button id="a4" type="button"  class="button">编辑资料补充</button>-->
                                <!--</td>-->
                            <!--</tr>-->
                        <!--</table>-->
                    <!--</div>-->
                <!--</div>-->
                <!--<div class="divbutton">-->
                    <!--<table>-->
                        <!--<tr>-->
                            <!--<td>-->
                                <!--<button id="save" class="button">保存</button>-->
                            <!--</td>-->
                            <!--<td>-->
                                <!--<button id="cancel" class="button">取消</button>-->
                            <!--</td>-->
                        <!--</tr>-->
                    <!--</table>-->
                <!--</div>-->


        </div>

     </div>
    <div class="footer-address">
        <div class="footer-address-all">
            <div class="footer-address-left">
                <div class="mm">地址：华中科技大学</div>
				   <div class="mm">电话查号台：010-62793001</div>
				   <div class="mm">华中科技大学•五只麦兜项目组荣誉出品</div>
            </div>
            <div class="footer-address-right">
                <div class="mm">京公网安备 110402430053 号</div>
            </div>
        </div>
    </div>
</div>

</body>
</html>