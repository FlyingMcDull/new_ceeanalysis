<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
	
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<base href="">
	<title>高考志愿填报分析助手首页 ---CEEAnalysis</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!--<link href="css/manage.css" rel="stylesheet">-->
	<link rel="stylesheet" href="css/homepage.css">
	<link rel="stylesheet" href="css/font.css">
	<script src="js/jquery-2.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style type="text/css">
	
		.main-center-slide-nav .slide  .hh {
			width: 60px;
			height: 50px;
			float: left;
			display: block;
			position: relative;
			letter-spacing: 2px;
			text-indent: -9999px;
			background: url('images/bgs.png') no-repeat scroll 30px -386px transparent;
		}
	
		.main-header {
			width: 100%;
			height: 134px;
			background: url("images/footer.png") center center;
			position: relative;
		}

	</style>
	<script type="text/javascript">
	</script>
</head>
<body >
 <div class="home-template">
   <header class="main-header">
	   <div class="main-header-user">
		   <div class="main-header-userextra">
			   <div class="main-header-userextra-save">
				   <a  id="favorites" class="fav-link" href="#">收藏此页</a>
			   </div>
		   </div>
		   <div class="main-header-usercenter">
			   <div class="main-header-usercenter-log">
			        <br/>
				   <bean:write name="username"/><span>您好</span><br>
				   <a target="_blank" href="login.html" title="登录界面">欢迎登录!</a>
			   </div>
		   </div>
	   </div>
	   <div class="container">
		   <div class="row">
			   <div class="col-md-12" style="padding-left: 0px;">
				   <a class="branding" href="#"  title="高考志愿填报网站">
				   <img src="images/logo.png" alt="高考系统" />
				   </a>
				   <!--<h1 style="display: inline-block;letter-spacing: 36px;padding-left:70px;font-weight: bolder;color: #349206;">高考志愿填报分析系统</h1>-->
			   </div>
		   </div>
	   </div>
   </header>
   <nav  class="main-navigation">
       <div class="container">
		   <div class="row">
			   <div class="col-sm-12">
				   <div class="navbar-header">
                        <span class="nav-toggle-button collapse" data-target="#main-menu">
                        <span class="sr-only">高考志愿填报分析助手导航</span>
                        <i class="fa fa-bars"></i>
                        </span>
				   </div>
				   <div class="collapse navbar-collapse" id="main-menu">
					   <ul class="menu">
						   <li id="home_page" class="nav-current" role="presentation"><a class="menu-name  selected" href="home_page.jsp">首页</a></li>
						   <li id="manage" role="presentation" ><a class="menu-name"  href="manage.jsp">用户管理</a></li>
						   <li id="estimated" role="presentation"><a class="menu-name" href=estimated.jsp>高考查询</a></li>
						   <li id="analysis" role="presentation"><a class="menu-name" href="analysis.jsp">专业评测</a></li>
						   <li id="rank_page" role="presentation"><a class="menu-name" href="rank_page.html">院校信息</a></li>
						   <li id="recommendation" role="presentation"><a class="menu-name" href="Recommendation.jsp">填报推荐</a></li>
					   </ul>
				   </div>
			   </div>
		   </div>
	   </div>
   </nav>
   <section id="mainContent" class="main-center">
       <div class="main-center-slide">
		   <div id="carousel-example-captions" class="carousel slide" data-ride="carousel">
			   <ol class="carousel-indicators">
				   <li data-target="#carousel-example-captions" data-slide-to="0" class=""></li>
				   <li data-target="#carousel-example-captions" data-slide-to="1" class="active"></li>
				   <li data-target="#carousel-example-captions" data-slide-to="2" class=""></li>
				   <li data-target="#carousel-example-captions" data-slide-to="3" class=""></li>
				   <li data-target="#carousel-example-captions" data-slide-to="4" class=""></li>
			   </ol>
			   <div class="carousel-inner" role="listbox">
				   <div class="item">
					    <a  href="http://www.tsinghua.edu.cn/publish/newthu/index.html" target="_blank">
					      <img data-src="images/1.jpg" alt="900x500" src="images/1.jpg" data-holder-rendered="true">
					    </a>
					       <div class="carousel-caption" >
							   <div id="explain1" style="width: 320px;height: 54px;margin:0 auto;background: #2A2214;border-radius: 5px;">
								   <h3>清华大学风貌</h3>
								   <h4>自强不息、厚德载物。</h4>
							   </div>

					       </div>
				   </div>
				   <div class="item active">
					   <a  href="http://www.whu.edu.cn" target="_blank">
						   <img data-src="images/3.jpg" alt="900x500" src="images/3.jpg" data-holder-rendered="true">
					   </a>
					   <div class="carousel-caption">
						   <div id="explain3" style="width: 320px;height: 54px;margin:0 auto;background:#0E419C;border-radius: 5px;">
							   <h3>武汉大学风貌</h3>
							   <h4>自强、弘毅、求是、拓新</h4>
						   </div>
					   </div>
				   </div>
				   <div class="item">
					   <a  href="http://www.pku.edu.cn/" target="_blank">
					       <img data-src="images/2.jpg" alt="900x500" src="images/2.jpg" data-holder-rendered="true">
					   </a>
					      <div class="carousel-caption">
							  <div id="explain2" style="width: 320px;height: 54px;margin:0 auto;background: #2D5428;border-radius: 5px;">
						         <h3>北京大学风貌</h3>
						         <h4>爱国进步民主科学。</h4>
							  </div>
					     </div>
				   </div>
				   <div class="item active">
					   <a  href="http://www.xmu.edu.cn/" target="_blank">
					      <img data-src="images/4.jpg" alt="900x500" src="images/4.jpg" data-holder-rendered="true">
					   </a>
					      <div class="carousel-caption">
							  <div id="explain4" style="width: 320px;height: 54px;margin:0 auto;background: #3E1A18;border-radius: 5px;">
						          <h3>厦门大学风貌</h3>
						          <h4>自强不息·止于至善。</h4>
							  </div>
					     </div>
				   </div>
				   <div class="item active">
					   <a  href="http://www.hust.edu.cn/" target="_blank">
					       <img data-src="images/5.jpg" alt="900x500" src="images/5.jpg" data-holder-rendered="true">
					   </a>
					      <div class="carousel-caption">
							  <div id="explain" style="width: 320px;height: 54px;margin:0 auto;background: #6D7069;border-radius: 5px;">
						         <h3>华中科技大学风貌</h3>
						         <h4>明德厚学、求是创新。</h4>
							  </div>
						  </div>
				   </div>
			   </div>
			   <a class="left carousel-control" href="#carousel-example-captions" role="button" data-slide="prev">
				   <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				   <span class="sr-only">Previous</span>
			   </a>
			   <a class="right carousel-control" href="#carousel-example-captions" role="button" data-slide="next">
				   <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				   <span class="sr-only">Next</span>
			   </a>
		   </div>
		   <div class="main-center-slide-nav" >
			   <ul class="slide">
				   <li>
					   <a class="hh" href=""></a>
					   <a class="kk" href="http://www.tsinghua.edu.cn/publish/newthu/index.html" target="_blank">清华大学</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
				   <li>
					   <a class="hh" href=""></a>
					   <a class="kk" href="http://www.pku.edu.cn/" target="_blank">北京大学</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
				   <li>
					   <a class="hh" href=""></a>
					   <a class="kk" href="http://www.fudan.edu.cn/index.html" target="_blank">复旦大学</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
				   <li>
					   <a class="hh" href=""></a>
					   <a class="kk" href="http://www.whu.edu.cn/" target="_blank">武汉大学</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
				   <li>
					   <a class="hh" href=""></a>
					   <a class="kk" href="http://www.xmu.edu.cn/" target="_blank">厦门大学</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
				   <li>
					   <a  class="hh" href=""></a>
					   <a class="kk jj"  href="http://www.hust.edu.cn/" target="_blank">华中科技大学</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
				   <li>
					   <a  class="hh" href=""style="background: url('images/bgs.png') no-repeat scroll 33px -489px transparent;"></a>
					   <a  class="kk" href="http://roll.edu.sina.com.cn/all/gk/kaoshi/index.shtml"  target="_blank">友情链接</a>
					   <!--两行均增加了跳转链接-->
					   <span class="icon"> ></span>
				   </li>
			   </ul>
		   </div>
	   </div>
	   <div class="main-center-mainWrap">
		   <section class="news">
				<h4  class="xinwen">
					<a  href="#" class="more" target="_blank">更多》</a>
					高考新闻
				</h4>
				<ul class="newslist">
					<li>
						 <div class="ydate">
							 <span>14</span>
							 2016.01
						 </div>
						 <div class="theme">
							 <h5>
								 <a href="http://edu.sina.com.cn/gaokao/2016-02-17/doc-ifxpmpqt1374322.shtml" target="_blank" class='hl'>2016本科“红牌专业”出炉</a>
								 <!--增加了链接跳转，修改了标题-->
							 </h5>
							 <p >据公布，2016年的红牌本科专业包括生物工程、美术学、生物科学、应用物理学、应用心理学、法学和音乐表演。这些专业为失业量较大、就业率较低、月收入较低且就业满意度较低，为高失业风险型专业。</p>
							 <!--修改了文字内容-->
						 </div>
					 </li>
					<li>
						<div class="ydate">
							<span>15</span>
							2016.01
						</div>
						<div class="theme">
							<h5>
								<a href="http://gaokao.eol.cn/jiang_su/dongtai/201602/t20160223_1368025.shtml" target="_blank"  class='hl'>江苏高考2021年实行“3+3”模式</a>
								<!--增加了跳转链接修改了标题-->
							</h5>
							<p>和现行高考方案相比，江苏省普通高考选考科目由现行的“6选2”调整为“6选3”，即由学生在思想政治、历史、地理、物理、化学、生物等6门科目中自主选择3门选考科目，并计入高校招生录取总成绩。</p>
							<!--修改了文字内容-->
						</div>
					</li>
					<li>
						<div class="ydate">
							<span>17</span>
							2016.01
						</div>
						<div class="theme">
							<h5>
								<a href="http://sx.neea.edu.cn/gkdt-gkgg/ssdt-11.htm" target="_blank"  class='hl'>2019年甘肃启动高考综合改革</a>
								<!--增加了跳转链接，修改了标题-->
							</h5>
							<p>综合考虑省情、教情，充分吸取和总结试点省份和先行先试省份改革经验和教训，本着稳中求进的原则，甘肃省将于2019年启动高考综合改革方案，具体方案将于2019年6月底前向社会公布。</p>
							<!--修改了文字内容-->
						</div>
					</li>
				</ul>
			</section>
		   <section class="focus">
				<h4 id="gg">高考焦点关注</h4>
				<ul class="focus-list">
                     <li class="focus-list-one" style="border-top: 2px solid  #01a157;">
						 <div class="theme">
							 <h5>
								 <a  class='hl' href="http://baike.baidu.com/link?url=XtO_BxVy8lNRL_XbhzdXPrBTT_jM305IkiKtPRTeQQvm_cJd-TXELOsixhCsqRy4xBrXCmasy5Hz1hMIRDOUkAdoS5oFlvUByozeNnwutEgIQ0Ne4x8TeP0wyg4sllLz-g1Ofr1mQ-JAY5SJoID1qjCaUJ21Fdym7zUIKjdtYcS" target="_blank">教育部2016年工作要点</a>
								 <!--增加了跳转链接，修改了标题-->
							 </h5>
							 <p>一、切实加强党的建设，全面维护教育系统和谐稳定<br/>
								 二、始终贯彻落实立德树人根本任务，着力提高教育质量<br/>
								 三、坚持改革创新，不断为教育事业发展注入动力活力<br/>
								 四、 坚持协调发展，不断优化教育结构<br/>
								 五、坚持共享发展，切实保障广大人民群众接受教育的权利<br/>
							 </p>
							 <!--修改了文字内容-->
						 </div>
					 </li>
					<li class="focus-list-one" style="border-top: 2px solid  #0E419C;">
						<div class="theme">
							<h5>
								<a  class='hl' href="http://baike.baidu.com/link?url=TSv24U8ZSTUMro7kaSwG7NwojG_O5Uu7AffChI5Cufco0dmP4fIBi7zJ0tHkqrwSw8H-OexfhvwfRVPwtvD6jK" target="_blank">什么是“特招”？</a>
								<!--增加了跳转链接修改了标题-->
							</h5>
							<p>特殊招生一般是指普通高校招生中的一些特殊类型或特殊政策。大多数特殊类型招生仍要求考生参加全国统一高考并按规定程序录取；个别特殊类型招生（如保送生、体育单招等）不需要参加全国统一高考，采取单独的选拔录取方式。</p>
							<!--修改了文字内容-->
						</div>
					</li>
					<li class="focus-list-one" style="border-top: 2px solid  #01a157;">
						<div class="theme">
							<h5>
								<a  class='hl' href="http://www.shmec.gov.cn/html/article/201602/86358.php" target="_blank">上海：2016体育现场确认2月28日举行</a>
								<!--增加了跳转链接，修改了标题-->
							</h5>
							<p>2016年上海市普通高校招生体育类专业(包括体育教育、社会体育指导与管理、休闲体育)统一考试现场确认将于2月28日在上海体育学院举行。考生所选的体育专项项目在2月28日一经确认不得更改。逾期未确认视为放弃普通高校体育类专业统一考试。</p>
							<!--修改了文字内容-->
						</div>
					</li>
					<li class="focus-list-one" style="border-top: 2px solid  #0E419C;">
						<div class="theme">
							<h5>
								<a  class='hl' href="http://www.798edu.com/article/cjwt/37464.html" target="_blank">艺术类专业</a>
								<!--增加了跳转链接,修改了标题-->
							</h5>
							<p>艺术类专业包括《普通高等学校本科专业目录(2012年)》中“艺术学门类”下设各专业（见下表），以及《普通高等学校高等职业教育(专科)专业目录(2015年)》中“艺术设计类”“表演艺术类”下设各专业和“民族文化类”“广播影视类”等部分专业。</p>
							<!--修改了文字内容-->
						</div>
					</li>
					<li class="focus-list-one" style="border-top: 2px solid  #01a157;">
						<div class="theme">
							<h5>
								<a  class='hl' href="http://ccn.people.com.cn/n1/2016/0229/c366510-28156838.html" target="_blank">宁夏：2016年教育民生计划发布</a>
								<!--增加了跳转链接修改了标题-->
							</h5>
							<p>一、	改善农村学校办学条件。<br/>
								二、实施农村学生营养改善计划。<br/>
								三、推进学前教育加快发展。<br/>
								四、实施家庭经济困难学生资助计划。<br/>
							</p>
							<!--修改了文字内容-->
						</div>
					</li>
					<li class="focus-list-one" style="border-top: 2px solid  #0E419C;">
						<div class="theme">
							<h5>
								<a  class='hl' href="http://gaokao.chsi.com.cn/gkxx/ss/201602/20160224/1523069584.html" target="_blank">海南：2017年启动高考改革</a>
								<!--增加了跳转链接，修改了标题-->
							</h5>
							<p>海南省启动高考综合改革的时间为2017年。这意味着2017年秋季进入普通高中的高一学生，将是我省高考综合改革的第一届学生。2016年秋季进入普通高中的学生以及目前正在就读普通高中的学生，仍将按照现行的高考政策参加高考和录取。</p>
							<!--修改了文字内容-->
						</div>
					</li>

				</ul>
			</section>
	   </div>
   </section>
   <footer class="footer">
	   <div class="footer-list">
		   <div class="footer-connection">
               <div class="footer-connection-all">
				   <div id="left-top">
					   <h4>常用链接</h4>
				   </div>
				   <div id="left-bottom">
					   <ul class="left-bottom-ul">
						   <li><a href="http://gaokao.chsi.com.cn/z/gkbmfslq/index.jsp" target="_blank">阳光高考</a></li>
						   <li><a href="http://gaokao.chsi.com.cn/zsgs/zhangcheng/listVerifedZszc--method-index,lb-1.dhtml" target="_blank">招生章程</a></li>
						   <li><a href="http://gaokao.chsi.com.cn/zyk/zybk/" target="_blank">专业知识库</a></li>
						   <li><a href="http://gaokao.chsi.com.cn/gkzt/gktbzy2014" target="_blank">填报指南</a></li>
						   <li><a href="http://gaokao.chsi.com.cn/sch/search--ss-on,option-qg,searchType-1.dhtml" target="_blank">院校信息</a></li>
						   <li><a href="http://baike.baidu.com/link?url=8nqLEMrZBQsOf_HuBDTuPOpBwVYh8vPmyYj5lq1wxW6rKx-bxQOprsDqqfu67Sx5l9TZdBtOFWOFrr23YwmfdK" target="_blank">百度更多</a></li>
					   </ul>
				   </div>
			   </div>
			   <div class="footer-connection-all">
				   <div class="footer-connection-photo">
					   <a href="#" target="_blank">
						   <img src="images/bei.jpg">
					   </a>
				   </div>
				   <div class="footer-connection-photo" >
					   <a href="#" target="_blank">
						   <img src="images/qing.jpg">
					   </a>
				   </div>
				   <div class="footer-connection-photo" >
					   <a href="#" target="_blank">
						   <img src="images/hua.jpg">
					   </a>
				   </div>
				   <div class="footer-connection-photo" >
					   <a href="#" target="_blank">
						   <img src="images/xia.jpg">
					   </a>
				   </div>
			   </div>
		   </div>
	   </div>
	   <div class="footer-address">
		   <div class="footer-address-all">
			   <div class="footer-address-left">
			       <div class="mm">地址：华中科技大学</div>
				   <div class="mm">电话查号台：010-62793001</div>
				   <div class="mm">华中科技大学•五只麦兜项目组荣誉出品</div>
			   </div>
			   <div class="footer-address-right">
				   <div class="mm">京公网安备 110402430053 号</div>
			   </div>
		   </div>
	   </div>
   </footer>
 </div>
</body>
</html>