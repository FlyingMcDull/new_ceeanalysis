<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>success</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
  </head>
  
  <body>
    success <br>
    firstBatch:<%= request.getSession().getAttribute("firstBatch") %><br>
    secondBatch:<%= request.getSession().getAttribute("secondBatch") %><br>
    thirdBatch:<%= request.getSession().getAttribute("thirdBatch") %>
  </body>
</html>
