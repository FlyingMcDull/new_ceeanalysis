<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>院校推荐 ---CEEAnalysis</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/manage.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/input.css" rel="stylesheet">
    <style type="text/css">
    
        #header {
            width: 100%;
            height: 134px;
            /*background-color: #f5f5f5;*/
            background: url("images/footer.png") center center;
            position: relative;
        }

        #container #mainContent #content {
            float: left;
            width: 80%;
            height: 100%;
        }

        #container #mainContent {
            width: 1000px;
            margin-left: auto;
            margin-right: auto;
            height: 1200px;
            margin-top: 50px;
        }
    
        #container #mainContent #sidebar .radius {
            border-width: 1px;
            border-style: solid;
            border-color: rgb(141, 143, 139);
            border-radius: 3px;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
            width: 180px;
            line-height: 75px;
            font-size: 21px;
            text-align: center;
            background-color: #337ab7;

        }
    
        #container #mainContent #content #manageform #content2 .content2_p {
            margin-top: 15px;
            text-align: center;
            font-size: 16px;
        }
    
        #container #mainContent #content #manageform #content1 .content1_span {
            margin-top: 25px;
            display: block;
            margin-bottom: 25px;
        }
    
        #container #mainContent #content #manageform #content1 #sex span {
            display: block;
            margin-bottom: 40px;
        }
    
        #container #mainContent #content #manageform #content1 .content1_p {
            border-bottom-width: 2px;
            border-bottom-style: dotted;
            border-bottom-color: #CCC;
            height: 150px;
            line-height: 70px;
            margin-top: 15px;
        }

        .content2_p {
            padding-left: 10px;
            padding-top: 5px;
            border-bottom-width: 2px;
            border-bottom-style: dotted;
            border-bottom-color: #CCC;
            height: 120px;
            margin-top: 15px;
        }

        .content3_p {
            padding: 10px;
            /*border-bottom-width: 2px;*/
            /*border-bottom-style: solid;*/
            /*border-bottom-color: #CCC;*/
            height: 210px;
            margin-top: 50px;
        }

        #container #mainContent #content #manageform #content1 {
            font-size: 16px;
            display: block;
            margin: 20px;
            padding-left: 20px;
        }

        #container #buttom {
            background-color: #FFF;
            width: 100%;
            height: 70px;
            margin-top: 50px;
        }
    
        #container #mainContent #content #manageform  .submit_btn {
            line-height: 25px;
            height: 39px;
            padding-left: 10px;
            padding-right: 10px;
            color:white;
            background-color: #66CCCC;
            font-size: 21px;
            font-weight: bold;
            font-family: Arial;
            background: -webkit-gradient(linear, left top, left bottom, color-start(0.05, #79bbff), color-stop(1, #378de5));
            background: -moz-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background: -o-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background: -ms-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background: linear-gradient(to bottom, #79bbff 5%, #378de5 100%);
            filter:progid: DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5',GradientType=0);
            border: 1px solid #84bbf3;
            -webkit-border-top-left-radius: 6px;
            -moz-border-radius-topleft: 6px;
            border-top-left-radius: 6px;
            -webkit-border-top-right-radius: 6px;
            -moz-border-radius-topright: 6px;
            border-top-right-radius: 6px;
            -webkit-border-bottom-left-radius: 6px;
            -moz-border-radius-bottomleft: 6px;
            border-bottom-left-radius: 6px;
            -webkit-border-bottom-right-radius: 6px;
            -moz-border-radius-bottomright: 6px;
            border-bottom-right-radius: 6px;
            -moz-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            -webkit-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            text-align: center;
            display: inline-block;
            text-decoration: none;
            background-position: top;
        }

        #content2 {
            text-align: center;
            border-top: solid 2px #d3d3d3;
            margin: 20px;
            padding-top: 40px;
            padding-left: 20px;
            padding-right: 20px;
        }

        #container #mainContent #content #manageform #content2 {
            text-align: center;
            border-top: solid 2px #d3d3d3;
            margin: 20px;
            padding-top: 40px;
            padding-left: 20px;
            padding-right: 20px;

        }

        #container #mainContent #content #manageform  .submit_btn:hover {
            background-color: #f5f5f5;
            background: -webkit-gradient(linear, left top, left bottom, color-start(0.05, #378de5), color-stop(1, #79bbff));
            background: -moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background: -o-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background: -ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
            background: -webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
            filter:progid: DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
        }

        #container #mainContent #sidebar {
            float: left;
            width: 18%;
            height: 100%;
            border-right: solid 2px #d3d3d3;
            margin: 0px;
        }

        table{
            border-collapse:collapse;
            border-spacing: 0;
            border-left: 1px solid #888;
            border-top: 1px solid #888;
            background: #FFF;
        }

        th,td {
            border-right: 1px solid #888;
            border-bottom: 1px solid #888;
            padding: 5px 15px;
        }

        th {
            font-weight: bold;
            background: #FFF;
        }

        td {
            width: 120px;
            height: 50px
        }

        #container #mainContent #content #manageform #content3 {
            text-align: center;
            padding-top: 30px;
            margin: 20px;
        }

        #container #mainContent #content #manageform  {
            border-bottom-width: 2px;
            border-bottom-style: dotted;
            border-bottom-color: #CCC;
        }

        #container #mainContent #content #manageform #content1 p,#container #mainContent #content #manageform #content1 #sex p {
            height: 50px;
            margin-top: 15px;
            padding: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
        }

        #container #mainContent #content #manageform #content1 #sex {
            margin-top: 25px;
        }
        .tip {
            color: #aaaaaa;
            font-size: 18px;
        }
        .title {
            font-size: 18px;
            line-height: 40px;
        }
        .score {
            font-size: 12px;
            height: 33px;
            width: 180px;
            padding-left: 6px;
            line-height: 33px;
            border: 3px;
            background-color: #f8efc0;
            border-color: #3FA3C1;
            border-radius: 2px;
            box-shadow: 3px 3px 3px  rgba(0,0,0,0.1) inset;
        }

    </style>


    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/create.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("button").click(function(){

                //获取省份元素
                var province = $("#province").val();

                //判断必填项是否为空
                var score= $("#score").val();
                if(score==""){
                    alert("分数不能为空！");
                    return false;
                }
                var scorenum = parseInt(score);
                if(scorenum<0||scorenum>750){
                    alert("请输入正确的分数！");
                    return false;
                }

                if(province=="请选择省份"){
                    alert("请选择正确的省份！");
                    return false;
                }

                //封装要上传的数据并异步传给后台
                var formdata = {
                    score: score,
                    province:province
                }


                $.ajax({
                    type: "POST",
                    url:"",
                    data:formdata,

                    success: function(data)
                    {
                        //在异步提交成功后要做的操作
                        $('#content2').load("introduce/subRecomendation.html");
                    }
                });
                return false;
            })
        });
    </script>

</head>
<body>
<div id="container">
    <div id="header">
        <div id="platform-header">
            <div id="platform-user">
                <div class="top-show">
                    <ul>
                        <li><a href="javascript:void(0);" data-src="" >收藏此页</a></li>
                    </ul>
                </div>
                <div class="top-show" style="float: right;">
                    <a href="login.html" target="_blank">退出登录</a>

                </div>
            </div>
        </div>
        <div id="logo-header">
            <div id="logo-left">
                <img src="images/logo.png" alt="高考系统">
            </div>
            <div id="logo-right"></div>
        </div>
    </div>


    <nav  class="main-navigation">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 hdcolor">
                    <div class="navbar-header">
                        <span class="nav-toggle-button collapse" data-target="#main-menu">
                        <span class="sr-only">高考志愿填报分析助手导航</span>
                        <i class="fa fa-bars"></i>
                        </span>
                    </div>
                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="menu">
                            <li id="home_page" class="nav-current" role="presentation"><a class="menu-name" href="home_page.jsp">首页</a></li>
                            <li id="manage" role="presentation" ><a class="menu-name"  href="manage.jsp">用户管理</a></li>
                            <li id="estimated" role="presentation"><a class="menu-name" href=estimated.jsp>高考查询</a></li>
                            <li id="analysis" role="presentation"><a class="menu-name" href="analysis.jsp">专业评测</a></li>
                            <li id="rank_page" role="presentation"><a class="menu-name" href="rank_page.html">院校信息</a></li>
                            <li id="recommendation" role="presentation"><a class="menu-name  selected" href="Recommendation.jsp">填报推荐</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="mainContent">
        <div id="sidebar">
            <div class="sideframe"><p class="radius" style="margin-top: 90px">是否完成估分</p></div>
            <div class="sideframe"><p class="radius" style="margin-top: 130px">选择报考地区</p></div>
            <!--<div class="sideframe"><p class="radius" style="margin-top: 110px">选择学校类型</p></div>-->
            <!--<p class="radius" style="margin-top: 120px">性别</p>-->
            <div class="sideframe"><p class="radius" style="margin-top: 450px">推荐结果</p></div>
        </div>
        <div id="content">
            <!--<form id="manageform" class="manageform" method="post">-->
                <div id="manageform" class="manageform">
                <div id="content1">
                    <div style="height: auto">
                    <span class="tip">请依次填写下列信息，我们将根据您的选择推荐合适的学校</span>
                    <p class="content1_p">
                        <span class="title">否:  <img src="images/arrow.png"\>
                        <a href="estimated.jsp" class="">返回估分页面</a>
                        </span>
             <br/>
                        <span class="title">是:  <img src="images/arrow.png"\>
                        <input id="score" type="text" class="score" placeholder="请输入您估计的分数">
                        </span>
                    </p>
                    </div>
                    <!--<div class="content2_p">-->
                    <!--<p >-->
                        <!--<span class="greybg">全部</span>-->
             <!--&nbsp; &nbsp; &nbsp; &nbsp;<span>-->
           <!--西北 <input type="checkbox" name="1" value="西北" checked>-->
           <!--东北 <input type="checkbox" name="1" value="东北">-->
           <!--华北 <input type="checkbox" name="1" value="华北">-->
           <!--华中 <input type="checkbox" name="1" value="华中">-->
           <!--西南 <input type="checkbox" name="1" value="西南">-->
           <!--东南 <input type="checkbox" name="1" value="东南">-->
           <!--南部 <input type="checkbox" name="1" value="南部">-->
            <!--</span>-->
                    <!--</p>-->
                    <!--</div>-->

                    <div id="choice_ablity" class="content3_p">
                        <!--<p>-->
                            <!--<span class="greybg">全部</span>-->
                            <!--&nbsp; &nbsp; &nbsp; &nbsp;<span>-->
           <!--综合 <input type="checkbox" name="2" value="综合" checked>-->
           <!--理工 <input type="checkbox" name="2" value="理工">-->
           <!--农林 <input type="checkbox" name="2" value="农林">-->
           <!--医药 <input type="checkbox" name="2" value="医药">-->
           <!--师范 <input type="checkbox" name="2" value="师范">-->
           <!--语言 <input type="checkbox" name="2" value="语言">-->
           <!--艺术 <input type="checkbox" name="2" value="艺术">-->
            <!--</span>-->
                        <!--</p>-->
                         <span class="title">您要报考的地区是:  <img src="images/arrow.png"\>
                        </span>
                        <select id="province" class="note select2" style="color: #aaaaaa;height: 40px;font-size: 18px;padding-left: 0px;">
                            <option value ="请选择省份"><em class="hide input">请选择省份</em></option>
                            <option value ="北京市">
                                北京市 </option><option value ="天津市">
                            天津市 </option><option value ="上海市">
                            上海市 </option><option value ="重庆市">
                            重庆市 </option><option value ="河北省">
                            河北省 </option><option value ="山西省">
                            山西省 </option><option value ="辽宁省">
                            辽宁省 </option><option value ="吉林省">
                            吉林省 </option><option value ="黑龙江省">
                            黑龙江省</option><option value ="江苏省">
                            江苏省 </option><option value ="浙江省">
                            浙江省 </option><option value ="安徽省">
                            安徽省 </option><option value ="福建省">
                            福建省 </option><option value ="江西省">
                            江西省 </option><option value ="山东省">
                            山东省 </option><option value ="河南省">
                            河南省 </option><option value ="湖北省">
                            湖北省 </option><option value ="湖南省">
                            湖南省 </option><option value ="广东省">
                            广东省 </option><option value ="海南省">
                            海南省 </option><option value ="四川省">
                            四川省 </option><option value ="贵州省">
                            贵州省 </option><option value ="云南省">
                            云南省 </option><option value ="陕西省">
                            陕西省 </option><option value ="甘肃省">
                            甘肃省 </option><option value ="青海省">
                            青海省 </option><option value ="台湾省">
                            台湾省 </option><option value ="广西壮族自治区">
                            广西壮族自治区</option><option value ="内蒙古自治区">
                            内蒙古自治区</option><option value ="西藏自治区">
                            西藏自治区</option><option value ="宁夏回族自治区">
                            宁夏回族自治区 </option><option value ="新疆维吾尔自治区">
                            新疆维吾尔自治区</option><option value ="香港特别行政区">
                            香港特别行政区</option><option value ="澳门特别行政区">
                            澳门特别行政区</option>
                        </select>


                        <p align="center" style="margin: 50px ">
                        <span style="display: block;  ">
                            <button value="提交" class="button3">提交</button>
                        </span >
                        </p>
                    </div>
                    <!--<div id="sex">-->
                        <!--<p > <input type="radio" name="sex" value="male" checked>男</p>-->
                        <!--<p  ><input type="radio" name="sex" value="female" checked>女</p>-->
                        <!--<span class="ablity_span"><input type="button"value="下一步" class="submit_btn" style="float:right"></span>-->
                    <!--</div>-->

                </div>

                <div id="content2" >
                    <div id="content2p">

                    </div>
                    <!--<div class="unix1">-->
                        <!--<p>武汉大学</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>学校代码：10486</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>属性：985,教育部直属，全国重点大学</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>简介：共有本科生31886人、硕士研究生16625人、博士研究生6741人、留学生1577人，教职工7760人</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                    <!--</div>-->
                    <!--<div class="unix2">-->
                        <!--<p>华中科技大学</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>学校代码：10486</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>属性：985,教育部直属，全国重点大学</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>简介：共有各类在校生总计56829人（其中本科生31886人、-->
                            <!--硕士研究生16625人、博士研究生6741人、留学生1577人），教职工7760人（其中专任教师3737人）</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                    <!--</div>-->
                    <!--<div class="unix1">-->
                        <!--<p>北京大学</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>学校代码：10001</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>属性：C9，TOP2</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                        <!--<p>简介：在校本科生32449人，研究生22837人，博士研究生6445人、硕士研究生16392人，各类留学生1745人。</p>-->
                        <!--&lt;!&ndash;&ndash;&gt;-->
                    <!--</div>-->
                </div>


                </div>
            <!--</form>-->
        </div>

    </div>

    <div id="buttom">
        <div class="footer-address">
            <div class="footer-address-all">
                <div class="footer-address-left">
                    <div class="mm">地址：华中科技大学</div>
				   <div class="mm">电话查号台：010-62793001</div>
				   <div class="mm">华中科技大学•五只麦兜项目组荣誉出品</div>
                </div>
                <div class="footer-address-right">
                    <div class="mm">京公网安备 110402430053 号</div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>