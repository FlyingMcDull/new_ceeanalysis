<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>专业推荐</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<h3>适合您的专业：</h3>
    <table border = "2">
    	<tr ancentre>
    		<td>专业名</td>
    		<td>学前准备</td>
    		<td>专业介绍</td>
    		<td>就业情况</td>
    	</tr>
    	<logic:iterate id="major" name="majors">
    	<tr>
    		<td><bean:write name="major" property="majName"/></td>
    		<td><bean:write name="major" property="majPre"/></td>
    		<td><bean:write name="major" property="majInt"/></td>
    		<td><bean:write name="major" property="majEmp"/></td>
    	</tr>
    	</logic:iterate>
    	
    </table>
  </body>
</html>
