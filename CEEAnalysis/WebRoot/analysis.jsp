<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>专业测评 ---CEEAnalysis</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/manage.css" rel="stylesheet">
    <link href="css/input.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet">
    <style type="text/css">
        #header{
            width: 100%;
            height: 134px;
            background: url("images/footer.png") center center;
            position: relative;
        }

        .botboder{
            border-bottom-width: 2px;
            border-bottom-style: dotted;
            border-bottom-color: #CCC;
        }

        #container #mainContent #content
        {
            float: left;
            width: 80%;
        }

        #container #mainContent {
            width: 1000px;
            margin-left: auto;
            margin-right: auto;
            height: 1200px;
            margin-top: 50px;
        }

        #container #mainContent #sidebar .radius {
            border-width: 1px;
            border-style: solid;
            border-color: rgb(141, 143, 139);
            border-radius: 3px;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-left-radius: 3px;
            border-bottom-right-radius: 3px;
            width: 180px;
            line-height: 75px;
            font-size: 25px;
            text-align: center;
            background-color: #337ab7;

        }
    
        #container #mainContent #content #manageform #content2 .content2_p {
            margin-top: 15px;
            text-align: center;
            font-size: 16px;
        }
    
        #container #mainContent #content #manageform #content1 .content1_span {
            margin-top: 25px;
            display: block;
            margin-bottom: 25px;
        }
    
        #container #mainContent #content #manageform #content1 #sex span
        {
            display: block;
            margin-bottom: 40px;
        }
    
        #container #mainContent #content #manageform #content1 .content1_p {
            border-bottom-width: 2px;
            border-bottom-style: dotted;
            border-bottom-color: #CCC;
            height: 100px;
            margin-top: 15px;
        }

        #container #mainContent #content #manageform #content1 {
            font-size: 16px;
            display:block;
            margin: 20px;
            padding-left: 20px;
        }
    
        #container #buttom {
            background-color: #FFF;
            width: 100%;
            height: 70px;
            margin-top: 50px;
        }
        #container #mainContent #content #manageform  .submit_btn {
            line-height: 25px;
            height: 39px;
            padding-left: 10px;
            padding-right: 10px;
            color:white;
            background-color: #66CCCC;
            font-size: 21px;
            font-weight: bold;
            font-family: Arial;
            background: -webkit-gradient(linear, left top, left bottom, color-start(0.05, #79bbff), color-stop(1, #378de5));
            background: -moz-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background: -o-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background: -ms-linear-gradient(top, #79bbff 5%, #378de5 100%);
            background: linear-gradient(to bottom, #79bbff 5%, #378de5 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5',GradientType=0);
            border: 1px solid #84bbf3;
            -webkit-border-top-left-radius: 6px;
            -moz-border-radius-topleft: 6px;
            border-top-left-radius: 6px;
            -webkit-border-top-right-radius: 6px;
            -moz-border-radius-topright: 6px;
            border-top-right-radius: 6px;
            -webkit-border-bottom-left-radius: 6px;
            -moz-border-radius-bottomleft: 6px;
            border-bottom-left-radius: 6px;
            -webkit-border-bottom-right-radius: 6px;
            -moz-border-radius-bottomright: 6px;
            border-bottom-right-radius: 6px;
            -moz-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            -webkit-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            box-shadow: inset 0px 1px 0px 0px #bbdaf7;
            text-align: center;
            display: inline-block;
            text-decoration: none;
            background-position: top;
        }


        #container #mainContent #content #manageform #content2 {
            border-top:solid 2px #d3d3d3;
            /*margin: 0px;*/
            /*padding-top: 0px;*/
            /*padding-left: 20px;*/
            /*padding-right: 20px;*/
        }

        #container #mainContent #content #manageform  .submit_btn:hover {
            background-color:#f5f5f5;
            background:-webkit-gradient(linear, left top, left bottom, color-start(0.05, #378de5), color-stop(1, #79bbff));
            background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
            background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
            background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
        }

        #container #mainContent #sidebar {
            float: left;
            width: 18%;
            height: 100%;
            border-right: solid 2px #d3d3d3;
            margin: 0px;
        }

        table{
            border-collapse:collapse;
            border-spacing: 0;
            border-left: 1px solid #888;
            border-top: 1px solid #888;
            background:#FFF;
        }

        th,td {
            border-right: 1px solid #888;
            border-bottom: 1px solid #888;
            padding: 5px 15px;
    }

        th {
            font-weight:bold;
            background:#FFF;
        }

        td {
            width: 120px;
            height: 50px
        }

        #container #mainContent #content #manageform #content3 {
            text-align: center;
            padding-top: 30px;
            margin: 20px;
        }

        #zhuanye {
            height: 435px;
            width: 950px;
            margin-top: 10px;
            margin-left: 20px;
        }

        #container #mainContent #content #manageform #content1 #choice_ablity {
            border-bottom-width: 2px;
            border-bottom-style: dotted;
            border-bottom-color: #CCC;
        }

        #container #mainContent #content #manageform #content1 #choice_ablity p,#container #mainContent #content #manageform #content1 #sex p {
            height: 50px;
            margin-top: 15px;
            padding: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 0px;
        }

        #container #mainContent #content #manageform #content1 #sex {
            margin-top: 25px;
        }
    </style>
    <link href="css/footer.css" rel="stylesheet">

    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/echarts.js"></script>
	<script src="js/Check.js"></script>
    <script src="js/manage.js"></script>

    <script type="text/javascript">
     	

     $(document).ready(function() {
//使用jQuery异步提交表单
            $('button').click(function() {

                var subject = $("#select").val();
                var area = "";
                var work = "";
                var hand = $("input:radio[name='majPra']:checked").val();
                var talk = $("input:radio[name='majExp']:checked").val();
                var write = $("input:radio[name='majWri']:checked").val();
                var sex = $("input:radio[name='majGen']:checked").val();


                $("input:checkbox[name='majSub']:checked").each(function(){
                    area+=$(this).val()+",";
                });
                    area=area.substring(0, area.length-1);
                $("input:checkbox[name='jname']:checked").each(function(){
                    work+=$(this).val()+",";
                });
                work=work.substring(0, work.length-1);


                
                var formdata = {
                    
                        "subject":subject,
                            "area":area,
                            "work":work,
                            "hand":hand,
                            "talk":talk,
                            "write":write,
                            "sex":sex
                        
                   
                };

               
                $.ajax({
                    url: "majorPre.do?method=findMajor",
                    data: formdata,
                    type: "POST",
                    success: function(data){
/*                     	console.log(123); */
/*                     	var json = eval("("+data+")"); */
           
/*                      	var score = JSON.parse(data);
                     	for(var i=0;i<5;i++)
                         console.log(score[i].majName); */
                         console.log(data);
                        var legendData = ['基础医学','护理学','临床医学','药学','生物工程'];
                        
/*                         for (var i = 0; i < data.length; i++) {
                        	console.log(data[i].majName);
                        	legendData.push(data[i].majName);
                        	console.log(legendData);
                        } */
                     //在异步提交成功后要做的操作;
                     var myChart=echarts.init(document.getElementById('major'));
                        option = {
                            tooltip: {
                                trigger: 'item',
                                //formatter: "{a} <br/>{b}: {c} ({d}%)"
                            },
                            legend: {
                                orient: 'vertical',
                                x: 'left',
 //                               data: legendData
                                data:['基础医学','护理学','临床医学','药学','生物工程']
                            },
                            series: [
                                {
                                    name:'专业推荐',
                                    type:'pie',
                                    radius: ['50%', '70%'],
                                    avoidLabelOverlap: false,
                                    label: {
                                        normal: {
                                            show: false,
                                            position: 'center'
                                        },
                                        emphasis: {
                                            show: true,
                                            textStyle: {
                                                fontSize: '30',
                                                fontWeight: 'bold'
                                            }
                                        }
                                    },
                                    labelLine: {
                                        normal: {
                                            show: false
                                        }
                                    },
                                    data:[
                  						{value: 333, name:'基础医学'},
                						{value: 333, name:'护理学'},
                						{value: 333, name:'临床医学'},
                						{value: 333, name:'药学'},
                						{value: 333, name:'生物工程'}
                					]
                                }
                            ]
                        };

                        myChart.setOption(option);
//                        $('#zhuanye').load("introduce/RecResult.html", function() {
//                            alert("Load was performed.");
//                        });
                        //window.location('success.html');
                        //alert(formdata+'Success!');
                    },
                    error:function(){
                    	console.log(1111);
                    }
                });
                return false;
            });
        });
    </script>


</head>
<body>
<div id="container">
    <div id="header">
        <div id="platform-header">
            <div id="platform-user">
                <div class="top-show">
                    <ul>
                        <li><a href="javascript:void(0);" data-src="" >收藏此页</a></li>
                    </ul>
                </div>
                <div class="top-show" style="float: right;">
                     <a  href="login.html" data-click="">退出登录</a>

                </div>
            </div>
        </div>
        <div id="logo-header">
            <div id="logo-left">
                <img src="images/logo.png" alt="高考系统">
            </div>
            <div id="logo-right"></div>
        </div>
    </div>


    <nav  class="main-navigation">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 hdcolor">
                    <div class="navbar-header">
                        <span class="nav-toggle-button collapse" data-target="#main-menu">
                        <span class="sr-only">高考志愿填报分析助手导航</span>
                        <i class="fa fa-bars"></i>
                        </span>
                    </div>
                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="menu">
                            <li id="home_page" class="nav-current" role="presentation"><a class="menu-name" href="home_page.jsp">首页</a></li>
                            <li id="manage" role="presentation" ><a class="menu-name"  href="manage.jsp">用户管理</a></li>
                            <li id="estimated" role="presentation"><a class="menu-name" href=estimated.jsp>高考查询</a></li>
                            <li id="analysis" role="presentation"><a class="menu-name  selected" href="analysis.jsp">专业评测</a></li>
                            <li id="rank_page" role="presentation"><a class="menu-name" href="rank_page.html">院校信息</a></li>
                            <li id="recommendation" role="presentation"><a class="menu-name" href="Recommendation.jsp">填报推荐</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="mainContent">
        <div id="sidebar">
            <div class="sideframe"><p class="radius" style="margin-top: 25px">分科</p></div>
            <div class="sideframe"><p class="radius" style="margin-top: 25px">领域</p></div>
            <div class="sideframe"><p class="radius" style="margin-top: 35px">工作类型</p></div>
            <div class="sideframe"><p class="radius" style="margin-top: 110px">能力</p></div>
            <div class="sideframe"><p class="radius" style="margin-top: 120px">性别</p></div>
            <div class="sideframe"><p class="radius" style="margin-top: 270px">专业推荐</p></div>
        </div>
        <div id="content">
<!--             <form id="manageform" method="post" class="manageform note botboder"> -->
                <div id="manageform" class="manageform note botboder">
                <div id="content1">
                    <p class="content1_p">您的分科是:　
                        <select id="select" name="majType" class="select2 little">
                        <option value="wen">文科</option>
                        <option value="li">理科</option>
                        </select>
                    </p>
                    <p class="content1_p">你感兴趣的领域是:　
                        <!--<select style="width: 120px">-->
                        <!--<option>金融</option>-->
                        <!--<option>教育</option>-->
                        <!--<option>建筑</option>-->
                        <!--<option>管理</option>-->
                        <!--<option>法律</option>-->
                        <!--<option>科学</option>-->
                        <!--<option>农业</option>-->
                        <!--<option>文学</option>-->
                        <!--<option>医学</option>-->
                        <!--<option>艺术</option>-->
                        <!--<option>时政</option>-->
                        <!--<option>工业</option>-->
                    <!--</select>-->
                        <br/><br/><span>
           金融 <input type="checkbox" name="majSub" value="finance" checked>
           教育 <input type="checkbox" name="majSub" value="education">
           建筑 <input type="checkbox" name="majSub" value="architecture">
           管理 <input type="checkbox" name="majSub" value="management">
           法律 <input type="checkbox" name="majSub" value="law">
           科学 <input type="checkbox" name="majSub" value="science">
           农业 <input type="checkbox" name="majSub" value="agronomy">
           文学 <input type="checkbox" name="majSub" value="literature">
           医学 <input type="checkbox" name="majSub" value="medicine">
           艺术 <input type="checkbox" name="majSub" value="art">
           时政 <input type="checkbox" name="majSub" value="political">
           工业 <input type="checkbox" name="majSub" value="industry">
            </span>
                        <br>

                        <span class="content1_span"></span>
                    </p>

                    <p class="content1_p">你今后想从事的工作类型是:　
                        <!--<select style="width: 120px">-->
                        <!--<option>工程师</option>-->
                        <!--<option>政治家</option>-->
                        <!--<option>企业家</option>-->
                        <!--<option>科学家</option>-->
                        <!--<option>教育家</option>-->
                        <!--<option>设计师</option>-->
                        <!--<option>研究员</option>-->
                        <!--<option>医生</option>-->
                        <!--<option>艺术家</option>-->
                    <!--</select>-->
                        <br/><br/><span>
           工程师 <input type="checkbox" name="jname" value="engineer" checked>
           政治家 <input type="checkbox" name="jname" value="politician">
           企业家 <input type="checkbox" name="jname" value="entrepreneur">
           科学家 <input type="checkbox" name="jname" value="scientist">
           教育家 <input type="checkbox" name="jname" value="educator">
           设计师 <input type="checkbox" name="jname" value="designer">
           研究员 <input type="checkbox" name="jname" value="researcher">
           医生 <input type="checkbox" name="jname" value="doctor">
           艺术家 <input type="checkbox" name="jname" value="artist">
            </span>
                        <br>
                        <span class="content1_span"></span>
                    </p>

                    <div id="choice_ablity" style="margin-top: 30px">
                        <p>
                            你的动手能力如何：
            <span>
            高 <input type="radio" name="majPra" value="3" checked>
           中 <input type="radio" name="majPra" value="2">
           低<input type="radio" name="majPra" value="1">
            </span>
                        </p>

                        <p>
                            你的表达能力如何：
            <span>
           高 <input type="radio" name="majExp" value="3" checked>
           中 <input type="radio" name="majExp" value="2">
           低<input type="radio" name="majExp" value="1">
            </span>
                        </p>

                        <p>
                            你的写作能力如何：
            <span>
          高 <input type="radio" name="majWri" value="3" checked>
           中 <input type="radio" name="majWri" value="2">
           低<input type="radio" name="majWri" value="1">
            </span>
                        </p>
                        <span style="display:block; margin-bottom: 20px;"></span >
                    </div>
                    <div id="sex" style="border-bottom: 1px; ">
                        <p > <input type="radio" name="majGen" value="2" checked>男</p>
                        <p  ><input type="radio" name="majGen" value="1">女</p>
                        <div class="ablity_span" align="center">
                            <!--<button  value="提交"  class="submit_btn" style="float:right">提交</button>-->
                             <button value="提交"  class="button3">提交</button>
                        </div>
                    </div>

                </div>
                </div>
<!--             </form> -->
            <!--<button id="a5" class="submit_btn" style="float:left">刷新</button>-->

                <div id="content2"></div>
                <div id="zhuanye">
                    <div id="major" style="width: 950px;height: 435px"></div>
                    <!--<script type="text/javascript">-->
                        <!--var myChart=echarts.init(document.getElementById('major'));-->
                        <!--option = {-->
                            <!--tooltip: {-->
                                <!--trigger: 'item',-->
                                <!--formatter: "{a} <br/>{b}: {c} ({d}%)"-->
                            <!--},-->
                            <!--legend: {-->
                                <!--orient: 'vertical',-->
                                <!--x: 'left',-->
                                <!--data:['基础医学','护理学','临床医学','药学','生物工程']-->
                            <!--},-->
                            <!--series: [-->
                                <!--{-->
                                    <!--name:'专业推荐',-->
                                    <!--type:'pie',-->
                                    <!--radius: ['50%', '70%'],-->
                                    <!--avoidLabelOverlap: false,-->
                                    <!--label: {-->
                                        <!--normal: {-->
                                            <!--show: false,-->
                                            <!--position: 'center'-->
                                        <!--},-->
                                        <!--emphasis: {-->
                                            <!--show: true,-->
                                            <!--textStyle: {-->
                                                <!--fontSize: '30',-->
                                                <!--fontWeight: 'bold'-->
                                            <!--}-->
                                        <!--}-->
                                    <!--},-->
                                    <!--labelLine: {-->
                                        <!--normal: {-->
                                            <!--show: false-->
                                        <!--}-->
                                    <!--},-->
                                    <!--data:[-->
                                        <!--{value: 300, name:'基础医学'},-->
                                        <!--{value: 300, name:'护理学'},-->
                                        <!--{value: 300, name:'临床医学'},-->
                                        <!--{value: 300, name:'药学'},-->
                                        <!--{value: 300, name:'生物工程'}-->
                                    <!--]-->
                                <!--}-->
                            <!--]-->
                        <!--};-->

                        <!--myChart.setOption(option);-->
                    <!--</script>-->
                </div>



        </div>

    </div>

    <div id="buttom">
    <div class="footer-address">
        <div class="footer-address-all">
            <div class="footer-address-left">
                <div class="mm">地址：华中科技大学</div>
				   <div class="mm">电话查号台：010-62793001</div>
				   <div class="mm">华中科技大学•五只麦兜项目组荣誉出品</div>
            </div>
            <div class="footer-address-right">
                <div class="mm">京公网安备 110402430053 号</div>
            </div>
        </div>
    </div>
</div>

</div>

</body>
</html>