/**
 * Created by DreamOkabe on 2016/3/1.
 */

$(document).ready(function(){
    //为导航栏添加局部跳转
    $("#a1").click(function(){
        $('#stable1').load("introduce/Personal.html #formone");
    })
    $("#a2").click(function(){
        $('#stable2').load("Reset.html #resetform");
    })
    $("#a3").click(function(){
        $('#stable3').load("introduce/bindEmail.html #myform");
    })
    $("#a4").click(function(){
        $('#stable4').load("introduce/extra.html #formone");
    })
    $("#a5").click(function(){
        $('#zhuanye').load("introduce/RecResult.html");
    })
})

/**
 * 返回管理界面
 */
function rtnManage(){
    $('body').load("manage.html");
}
