/**
 * Created by DreamOkabe on 2016/3/8.
 */
function createUni(){
    var a = "<p>名称：北京大学（简称：北大）</p>"+ "<p>位置：北京市</p>"+ "<p>属性：C9，TOP2</p>"+
        "<p>简介：在校本科生32449人，研究生22837人，博士研究生6445人、硕士研究生16392人，各类留学生1745人。</p>";
    var b = document.getElementById("content2");
    var unix = document.createElement("div");

    unix.className = "unix1";
    unix.innerHTML=a;
    b.appendChild(unix);
}

/**
 * 预测分数线
 */
function scoreline(){
    var myChart=echarts.init(document.getElementById('table'));
    var option = {
        title: {
            text: '本省/市分数线预测'
        },
        tooltip: {},
        legend: {
            data: ['文科','理科']
        },
        xAxis: {
            data: ["三本线", "二本线", "一本线"]
        },
        yAxis: {},
        series: [
            {
                name: '文科',
                type: 'bar',
                data: [450,490,520]
//                    data:[score.litlevel1,score.litlevel2,score.litlevel3]
            },
            {
                name:'理科',
                type:'bar',
                data:[490,530,570,]
//                    data:[score.siclevel1,score.siclevel2,score.siclevel3]
            }
        ]
    }
    
    myChart.setOption(option);
}


