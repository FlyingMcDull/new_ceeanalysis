/**
 * Created by DreamOkabe on 2016/3/2.
 */


/**
 * 登录(账号密码)验证
 */
function Logincheck(){
    //获取用户名元素以及用户名报错元素
    var username = $("#username");
    var usernamec = $(".Error");
    //获取密码元素以及密码报错元素
    var password = $("#password");
    var passwordc = $(".ReError");

    //判断必填项是否为空。为空，显示错误；不为空，不显示任何
    if (username.val().length == 0) {
        usernamec.html("*对不起，用户名不能为空！");
        return false;
    } else {
        usernamec.html("");
    }

    if (password.val().length == 0){
        passwordc.html("*对不起，密码不能为空！");
        return false;
    } else {
        passwordc.html("");
    }
 
    //封装要上传的数据并异步传给后台
    var formdata = "username:" +username.val()+ "," + "password:" +password.val();
    
    $.ajax({
        cache: false,
        type: "POST",
        url:"",
        data:formdata,
        async: false,
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}


/**
 * 个人基本资料验证
 * 必填项是否为空
 */
function checkPersonal(){
    //获取个人信息的必须元素：姓名、省份、城市、文理科
    var name = $('#name');
    var province = $('#province');
    var city = $('#city');
    var subject = $('#subject');
    
    //获取错误显示元素
    var Error1 = $('.Error1');
    var Error2 = $('.Error2');
    var Error3 = $('.Error3');
    
    //判断必填项是否为空。为空，显示错误；不为空，不显示任何。为空，显示错误；不为空，不显示任何
    if (name.val().length == 0) {
        Error1.html("*昵称不能为空！");
        return false;
    } else {
        Error1.html("");
    }
    
    if (city.val() == '请选择市区'){
        Error2.html("*请选择正确的市区！");
        return false;
    } else {
        Error2.html("");
    }
    
    if (province.val() == '请选择省份'){
        Error2.html("*请选择正确的省份！");
        return false;
    } else {
        Error2.html("");
    }
    
    if (subject.val() == '请选择科目'){
        Error3.html("*请选择正确的科目！");
        return false;
    } else {
        Error3.html("");
    }

    //封装要上传的数据并异步传给后台
    var formdata = "name:" +name.val()+ "," + "province:" +province.val()+ "," + "city:" +city+ "," +
        "subject:" +subject;
    
    $.ajax({
        type:"POST",
        url:"",
        timeout:12000,
        data:formdata,
        dataType:"json",
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}


/**
 * 修改密码验证
 */
function checkEdit(){
    var oldpw = $('#oldpw');
    var newpw = $('#newpw');
    var confpw = $('#confirmpw');

    //判断必填项是否为空。为空，显示错误；不为空，不显示任何
    if (oldpw.val().length == 0){
        $('.Error2').html('*原密码不能为空！');
        return false;
    } else {
        $('.Error2').html('');
    }
    if (newpw.val().length == 0){
        $('.Error3').html('*新密码不能为空！');
        return false;
    } else {
        $('.Error3').html('');
    }
    if (confpw.val().length == 0){
        $('.Error4').html('*确认密码不能为空！');
        return false;
    } else {
        $('.Error4').html('');
    }

    //判断两次密码是否相等
    if (newpw.val()!=confpw.val()){
        $('.Error4').html('*两次密码不同，请重新输入！');
        return false;
    } else {
        $('.Error4').html('');
    }

    //封装要上传的数据并异步传给后台
    var formdata = "oldpassword:" +oldpw.val()+ "," + "newpassword:" +newpw.val()+ "," +
        "confirmpassword:" +confpw.val();

    $.ajax({
        type:"POST",
        url:"",
        timeout:12000,
        data:formdata,
        dataType:"json",
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}

/**
 * 重置密码验证
 */
function checkReset(){
    //输入的新密码
    var newpw = $('#newpw');
    //输入的新的确认密码
    var confpw = $('#confirmpw');

    //判断必填项是否为空。为空，显示错误；不为空，不显示任何
    if (newpw.val().length == 0){
        $('.Error3').html('*新密码不能为空！');
        return false;
    } else {
        $('.Error3').html('');
    }
    if (confpw.val().length == 0){
        $('.Error4').html('*确认密码不能为空！');
        return false;
    } else {
        $('.Error4').html('');
    }

    //判断两次密码是否相等
    if (newpw.val()!=confpw.val()){
        $('.Error4').html('*两次密码不同，请重新输入！');
        return false;
    } else {
        $('.Error4').html('');
    }

    //封装要上传的数据并异步传给后台
    var formdata = "newpassword:" +newpw.val()+ "," + "confirmpassword:" +confpw.val();

    $.ajax({
        type:"POST",
        url:"",
        timeout:12000,
        data:formdata,
        dataType:"json",
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}

/**
 * 忘记密码验证
 */
function checkForget(){
    //获取电子邮件元素以及其错误显示元素
    var email = $("#email");
    var emailc = $(".ReError");

    //判断必填项是否为空。为空，显示错误；不为空，不显示任何
    if (email.val().length == 0){
        emailc.html("*对不起，用户名不能为空！");
        return false;
    }
    else {
        emailc.html("");
    }

    //封装要上传的数据并异步传给后台
    var formdata = "username:" +email.val();

    $.ajax({
        cache: false,
        type: "POST",
        url:"",
        data:formdata,
        async: false,
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}

/**
 * 用户注册验证
 */
function checkRegister(){
    //获取
    var pwd = $('#password');
    var repwd = $('#confirmpw');
    var username = $('#username');
    var province = $('#province');
    var subject = $('#subject');
    var email = $('#email');
    var Error1 = $('.Error1');
    var Error2 = $('.Error2');
    var Error3 = $('.Error3');
    var Error4 = $('.Error4');
    var Error5 = $('.Error5');
    var Error6 = $('.Error6');

    //判断必填项是否为空。为空，显示错误；不为空，不显示任何
    if (username.val().length == 0){
        Error2.html("*用户名不能为空！");
        return false;
    } else {
        Error2.html("");
    }
    if (pwd.val().length == 0){
        Error3.html("*密码不能为空！");
        return false;
    } else {
        Error3.html("");
    }
    if (repwd.val().length == 0){
        Error4.html("*密码不能为空！");
        return false;
    } else {
        Error4.html("");
    }

    //判断下拉框是否已经选择
    if (province.val() =="请选择省份"){
        Error5.html("*请选择正确的省份！");
        return false;
    } else {
        Error5.html("");
    }
    if (subject.val() =="请选择科目"){
        Error6.html("*请选择正确的科目！");
        return false;
    } else {
        Error6.html("");
    }
    if (email.val().length == 0) {
        Error1.html("*邮箱不能为空！");
        return false;
    } else {
        Error1.html("");
    }

    //判断两次密码是否相同
    if (pwd.val()!=repwd.val()){
        Error4.html('两次输入的密码不同，请重新输入！');
        return false;
    } else {
        Error4.html("");
    }

    //封装要上传的数据并异步传给后台
    var formdata = "username:" +username.val()+ "," + "password:" +pwd.val()+ "," + "province:" +
        province.val()+ "," + "subject:" +subject+ "email" +email.val();

    $.ajax({
        type:"POST",
        url:"",
        timeout:12000,
        data:formdata,
        dataType:"json",
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}


/**
 * 资料补充验证
 */
function checkPersonal(){
    //获取补充的个人信息元素及其错误显示元素
    var name = $('#name');
    var province = $('#province');
    var city = $('#city');
    var subject = $('#subject');
    var Error1 = $('.Error1');
    var Error2 = $('.Error2');
    var Error3 = $('.Error3');

    //判断必填项是否为空。为空，显示错误；不为空，不显示任何
    if (name.val().length == 0) {
        Error1.html("*昵称不能为空！");
        return false;
    } else {
        Error1.html("");
    }
    if (city.val() == '请选择市区'){
        Error2.html("*请选择正确的市区！");
        return false;
    } else {
        Error2.html("");
    }
    if (province.val() == '请选择省份'){
        Error2.html("*请选择正确的省份！");
        return false;
    } else {
        Error2.html("");
    }
    if (subject.val() == '请选择科目'){
        Error3.html("*请选择正确的科目！");
        return false;
    } else {
        Error3.html("");
    }

    //封装要上传的数据并异步传给后台
    var formdata = "name:" + name.val()+ "," + "province:" +province.val()+ "," + "city:" +city+ "," +
        "subject:" + subject;

    $.ajax({
        type:"POST",
        url:"",
        timeout:12000,
        data:formdata,
        dataType:"json",
        success:submitcheck(data,formdata),
        error:error()
    });

    return false;
}