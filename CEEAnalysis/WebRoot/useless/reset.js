/**
 * Created by DreamOkabe on 2016/2/29.
 */


//function checkEmpty(item){
//    if(item.val().length==0) {
//        Error1.html("*用户名不能为空！");
//        return false;
//    }else {
//        Error1.html("");
//    }
//}


function checkReset(){

    var oldpw = $('#oldpw');
    var newpw = $('#newpw');
    var confpw = $('#confirmpw');



    //判断必填项是否为空

    if(oldpw.val().length==0){
        $('.Error2').html('*原密码不能为空！');
        return false;
    }else {
        $('.Error2').html('');
    }
    if(newpw.val().length==0){
        $('.Error3').html('*新密码不能为空！');
        return false;
    }else {
        $('.Error3').html('');
    }
    if(confpw.val().length==0){
        $('.Error4').html('*确认密码不能为空！');
        return false;
    }else {
        $('.Error4').html('');
    }

    //判断两次密码是否相等
    if(newpw.val()!=confpw.val()){
        $('.Error4').html('*两次密码不同，请重新输入！');
        return false;
    }else {
        $('.Error4').html('');
    }

    var formdata = "oldpassword:"+oldpw.val()+","+
    "newpassword:"+newpw.val()+","+
    "confirmpassword:"+confpw.val();


    $.ajax({
        type:"POST",
        url:"",
        timeout:12000,
        data:formdata,
        dataType:"json",
        success:submitcheck(data,formdata),
        error:error()
    });

    return true;

}

function submitcheck(data,json){//登录成功后返回的数据
    if(data==0){
        $("#divError").html("原密码错误！");
    }else if(data==1){
        $("#resetform").submit();
        window.location.href('success.html');
        alert(json+'Success!');

    }else {
        alert("OK!");
    }
    return false;
}

function error(){
    alert("error!");
}