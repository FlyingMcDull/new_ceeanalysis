﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>专业评测</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
	 <html:form method="post" action="/majorPre.do?method=findMajor">
 		<h1>请选择下列选项</h1>
 		您想选的专业类别:<br>
		文<html:radio property="majType" value="文"/>
		理<html:radio property="majType" value="理"/><br><br>
		你想从事的领域（多选）：<br>
		金融<html:multibox property="majSub" value="金融"/>
		工业<html:multibox property="majSub" value="工业"/>
		教育<html:multibox property="majSub" value="教育"/><br><br>
  		您想从事的职业（多选）:<br>
		教育家<html:multibox property="jname" value="教育家"/>
		医生<html:multibox property="jname" value="医生"/>
		运动员<html:multibox property="jname" value="运动员"/>
		政治家<html:multibox property="jname" value="政治家"/>
		艺术家<html:multibox property="jname" value="艺术家"/>
		企业家<html:multibox property="jname" value="企业家"/>
		工程师<html:multibox property="jname" value="工程师"/><br><br>
		您的动手能力:<br>
		高<html:radio property="majPra" value="3"/>
		中<html:radio property="majPra" value="2"/>
		低<html:radio property="majPra" value="1"/><br><br>
		您的表达能力:<br>
		高<html:radio property="majExp" value="3"/>
		中<html:radio property="majExp" value="2"/>
		低<html:radio property="majExp" value="1"/><br><br>
		您的写作能力:<br>
		高<html:radio property="majWri" value="3"/>
		中<html:radio property="majWri" value="2"/>
		低<html:radio property="majWri" value="1"/><br><br>
		您的性别:<br>
		男<html:radio property="majGen" value="2"/>
		女<html:radio property="majGen" value="1"/><br><br>
		<html:submit>提交</html:submit><br>
	</html:form>
  </body>
</html>
