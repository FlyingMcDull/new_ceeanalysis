package com.hibernate.util;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Transaction;

import javax.transaction.SystemException;

import org.hibernate.Session;

import com.hibernate.persistence.Major;
import com.hibernate.persistence.Province;
import com.hibernate.persistence.University;
import com.hibernate.persistence.UserInf;

public class HibernateUtil {
	static private Session session;
	static GetHibernate hib = new GetHibernate();
	
	/**
	 * 用户登录
	 * @param 
	 * 	username用户名
	 * 	password密码
	 * @return 
	 * 	用户集合
	 */
	public static List FindUser(String username, String password) throws IllegalStateException, SystemException {
		Transaction tx = null;
		List list = null;
		session = hib.openSession();
		tx = session.beginTransaction();
		try {
			Query query = session.createQuery("from UserInf as u where u.uname =:username and u.upw =:password");
			query.setString("username", username);
			query.setString("password", password);
			list = query.list();
			tx.commit();
			hib.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return list;
	}
	
	/**
	 * 通过用户名获得用户对象集合
	 * @param 
	 * 	username用户名
	 * @return 
	 * 	用户集合
	 */
	public static List FindUser(String username){
		Transaction tx = null;
		List user = null;
		session = hib.openSession();
		tx = session.beginTransaction();
		try {
			Query query = session.createQuery("from UserInf as u where u.uname =:username");
			query.setString("username", username);
			user = query.list();
			tx.commit();
			hib.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return user;
	}
	
	/**
	 * sql语句拼接
	 * @param 
	 * 	keyName字段名
	 *  values属性
	 *  condition连接条件（and，or）
	 * @return 
	 * 	完整Sql语句
	 */
	public static String MultiQuerySql(String keyName, String[] values, String condition) {
		if (values == null) return "";
		
		StringBuilder sb = new StringBuilder(100);
		
		for (int i = 1; i < values.length; i++) {
			sb.append(String.format(" %s = '%s' %s "
					,keyName, values[i], condition));
		}
		if (values.length > 0) {
			sb.append(String.format(" %s = '%s' ", keyName, values[0]));
		}
		
		return sb.toString();
	}
	
	/**
	 * 通过专业所属类别，专业所属学科，想从事的职业，动手能力，表达能力，写作能力，性别得到专业集合
	 * @param 
	 * 	majType专业所属类别
	 * 	majSub专业所属学科
	 * 	jname想从事的职业
	 * 	majPra动手能力
	 * 	majExp表达能力
	 * 	majWri写作能力
	 * 	majGen性别
	 * @return 
	 * 	专业集合
	 */
	public static List<Major> FindMajor(String majType,String[] majSub,
			String[] jname,int majPra,int majExp,int majWri,int majGen){
		Transaction tx = null;
		List major = null;
		session = hib.openSession();
		tx = session.beginTransaction();
		try {
			String sql = "FROM Major WHERE ";
			String type1Sql = MultiQuerySql("majSub", majSub, "OR"),
				   type3Sql = MultiQuerySql("jName", jname, "OR");
			sql = String.format("%s majType = '%s' AND "
					+ " (%s) AND majPra <= %d AND majExp <= %d "
					+ " AND majWri <= %d AND (majGen = %d OR "
					+ " majGen = 0) AND jId IN (SELECT j.jid FROM Job as j "
					+ " WHERE %s)", sql, majType, type1Sql, majPra, 
					majExp, majWri, majGen, type3Sql);
			
			Query query = session.createQuery(sql);
			major = query.list();
			
			tx.commit();
			hib.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return major;
	}
	
	/**
	 * 通过省份id得到省份集合
	 * @param 
	 * 	pro_id 省份id
	 * @return 
	 * 	省份集合
	 */
	public static List findPro(Integer pro_id) {
		List list = null;
		session = hib.openSession();
		Transaction tx = session.beginTransaction();
		try {
			Query query = session.createQuery("from Province as p where p.proId =:pro_id");
			query.setInteger("pro_id", pro_id);
			list = query.list();
			tx.commit();
			hib.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return list;
	}
	
	/**
	 * 注册
	 * @param 
	 * 	username用户名
	 * 	password密码
	 * 	email邮箱
	 * 	type文理类别
	 * 	province省份对象
	 */
	public static boolean register(String username, String password, String email,
									String type, Province province) {
		session = hib.openSession();
		Transaction tx = session.beginTransaction();
		try {
			UserInf user = new UserInf(province, password, username, email, type);
			session.save(user);
			tx.commit();
			hib.closeSession(session);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			return false;
		}
	}
	
	/**
	 * 通过省份名字和分数推荐院校
	 * @param 
	 * 	proName省份名字
	 * 	uScore分数
	 * @return 
	 * 	院校集合
	 */
	public static List<University> FindUniversity(String proName,int uScore){
		Transaction tx = null;
		List university = null;
		session = hib.openSession();
		tx = session.beginTransaction();
		try {
			Calendar a=Calendar.getInstance(); 
			System.out.println(a.get(Calendar.YEAR));
			String year = Integer.toString(Calendar.YEAR);
			String sql = String.format("FROM University WHERE proId IN(SELECT p.proId FROM Province as p WHERE p.proName = '%s') "
					+ "AND uniId IN (SELECT r.uniId FROM Recruit as r WHERE (('%d'-(SELECT ts FROM Scores WHERE date = '%s')) > "
					+ "(SELECT avg(r.recSco-s.ts) FROM Recruit as r,Scores as s where r.date = s.date group by r.uniId)))",proName,uScore, year);
			Query query = session.createQuery(sql);
			university = query.list();
			tx.commit();
			hib.closeSession(session);
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return university;
	}
	
	/**
	 * 通过省份id和文理科类别得到分数线集合
	 * @param 
	 * 	proId 省份id
	 * 	type 文理科类别
	 * @return 
	 * 	分数线集合
	 */
	public static List getScores(int proId, String type) {
		session = hib.openSession();
		Transaction tx = session.beginTransaction();
		List list = null;
		try {
			Query query = session.createQuery("select u.uscore from UserInf as u,Province as p where u.province = p and p.proId =:proId and u.utype =:type");
			query.setInteger("proId", proId);
			query.setString("type", type);
			list = query.list();
			tx.commit();
			hib.closeSession(session);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			return list;
		}
	}
	
	/**
	 * 修改密码
	 * @param 
	 * 	userId 用户id
	 *  password 新密码
	 */
	public static boolean modPass(int userId, String password) {
		session = hib.openSession();
		Transaction tx = session.beginTransaction();
		try {
			Query query = session.createQuery("update UserInf set upw =:password where uid =:userId");
			query.setString("password", password);
			query.setInteger("userId", userId);
			query.executeUpdate();
			tx.commit();
			hib.closeSession(session);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			return false;
		}
	}
	
	/**
	 * password
	 * @param 
	 * 	userId 用户id
	 * 	email 新邮箱地址
	 */
	public static boolean modEmail(int userId, String email) {
		session = hib.openSession();
		Transaction tx = session.beginTransaction();
		try {
			Query query = session.createQuery("update UserInf set uemail =:email where uid =:userId");
			query.setString("email", email);
			query.setInteger("userId", userId);
			query.executeUpdate();
			tx.commit();
			hib.closeSession(session);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			return false;
		}
	}
}
