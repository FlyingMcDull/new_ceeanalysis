package com.hibernate.util;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.persistence.Admin;
import com.hibernate.persistence.City;
import com.hibernate.persistence.EstimateScore;
import com.hibernate.persistence.Job;
import com.hibernate.persistence.Major;
import com.hibernate.persistence.MajorScore;
import com.hibernate.persistence.MajorScoreId;
import com.hibernate.persistence.Paper;
import com.hibernate.persistence.Province;
import com.hibernate.persistence.Recruit;
import com.hibernate.persistence.RecruitId;
import com.hibernate.persistence.Scores;
import com.hibernate.persistence.ScoresId;
import com.hibernate.persistence.University;
import com.hibernate.persistence.UniversityMajor;
import com.hibernate.persistence.UniversityMajorId;
import com.hibernate.persistence.UniversityRank;
import com.hibernate.persistence.UniversityRankId;
import com.hibernate.persistence.UserInf;;

public class GetHibernate {
	private static SessionFactory sf = null;
	static{
		try {
			Configuration configuration = new Configuration().configure();
			sf = configuration.buildSessionFactory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Session openSession() {
		Session session = sf.openSession();
		return session;
	}
	
	public void closeSession(Session session) {
		if(session != null){
			session.close();
		}
	}
}
