package com.hibernate.persistence;

import java.util.HashSet;
import java.util.Set;

/**
 * City entity. @author MyEclipse Persistence Tools
 */

public class City implements java.io.Serializable {

	// Fields

	private Integer cid;
	private Province province;
	private String cname;
	private Set universities = new HashSet(0);

	// Constructors

	/** default constructor */
	public City() {
	}

	/** minimal constructor */
	public City(Integer cid, Province province, String cname) {
		this.cid = cid;
		this.province = province;
		this.cname = cname;
	}

	/** full constructor */
	public City(Integer cid, Province province, String cname, Set universities) {
		this.cid = cid;
		this.province = province;
		this.cname = cname;
		this.universities = universities;
	}

	// Property accessors

	public Integer getCid() {
		return this.cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getCname() {
		return this.cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public Set getUniversities() {
		return this.universities;
	}

	public void setUniversities(Set universities) {
		this.universities = universities;
	}

}