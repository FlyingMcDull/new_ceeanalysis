package com.hibernate.persistence;

import java.util.HashSet;
import java.util.Set;

/**
 * UserInf entity. @author MyEclipse Persistence Tools
 */

public class UserInf implements java.io.Serializable {

	// Fields

	private Integer uid;
	private Province province;
	private String upw;
	private String uname;
	private String uemail;
	private String utype;
	private Integer uscore;
	private Set estimateScores = new HashSet(0);

	// Constructors

	/** default constructor */
	public UserInf() {
	}

	/** minimal constructor */
	public UserInf(Province province, String upw, String uname, String uemail,
			String utype) {
		this.province = province;
		this.upw = upw;
		this.uname = uname;
		this.uemail = uemail;
		this.utype = utype;
	}

	/** full constructor */
	public UserInf(Province province, String upw, String uname, String uemail,
			String utype, Integer uscore, Set estimateScores) {
		this.province = province;
		this.upw = upw;
		this.uname = uname;
		this.uemail = uemail;
		this.utype = utype;
		this.uscore = uscore;
		this.estimateScores = estimateScores;
	}

	// Property accessors

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getUpw() {
		return this.upw;
	}

	public void setUpw(String upw) {
		this.upw = upw;
	}

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUemail() {
		return this.uemail;
	}

	public void setUemail(String uemail) {
		this.uemail = uemail;
	}

	public String getUtype() {
		return this.utype;
	}

	public void setUtype(String utype) {
		this.utype = utype;
	}

	public Integer getUscore() {
		return this.uscore;
	}

	public void setUscore(Integer uscore) {
		this.uscore = uscore;
	}

	public Set getEstimateScores() {
		return this.estimateScores;
	}

	public void setEstimateScores(Set estimateScores) {
		this.estimateScores = estimateScores;
	}

}