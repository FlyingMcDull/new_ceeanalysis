package com.hibernate.persistence;

/**
 * UniversityMajor entity. @author MyEclipse Persistence Tools
 */

public class UniversityMajor implements java.io.Serializable {

	// Fields

	private UniversityMajorId id;

	// Constructors

	/** default constructor */
	public UniversityMajor() {
	}

	/** full constructor */
	public UniversityMajor(UniversityMajorId id) {
		this.id = id;
	}

	// Property accessors

	public UniversityMajorId getId() {
		return this.id;
	}

	public void setId(UniversityMajorId id) {
		this.id = id;
	}

}