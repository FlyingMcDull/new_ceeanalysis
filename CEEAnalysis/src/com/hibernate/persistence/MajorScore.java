package com.hibernate.persistence;

/**
 * MajorScore entity. @author MyEclipse Persistence Tools
 */

public class MajorScore implements java.io.Serializable {

	// Fields

	private MajorScoreId id;
	private Integer majSco;

	// Constructors

	/** default constructor */
	public MajorScore() {
	}

	/** full constructor */
	public MajorScore(MajorScoreId id, Integer majSco) {
		this.id = id;
		this.majSco = majSco;
	}

	// Property accessors

	public MajorScoreId getId() {
		return this.id;
	}

	public void setId(MajorScoreId id) {
		this.id = id;
	}

	public Integer getMajSco() {
		return this.majSco;
	}

	public void setMajSco(Integer majSco) {
		this.majSco = majSco;
	}

}