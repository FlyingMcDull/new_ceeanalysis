package com.hibernate.persistence;

/**
 * UniversityRank entity. @author MyEclipse Persistence Tools
 */

public class UniversityRank implements java.io.Serializable {

	// Fields

	private UniversityRankId id;
	private Integer rank;

	// Constructors

	/** default constructor */
	public UniversityRank() {
	}

	/** full constructor */
	public UniversityRank(UniversityRankId id, Integer rank) {
		this.id = id;
		this.rank = rank;
	}

	// Property accessors

	public UniversityRankId getId() {
		return this.id;
	}

	public void setId(UniversityRankId id) {
		this.id = id;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

}