package com.hibernate.persistence;

import java.util.HashSet;
import java.util.Set;

/**
 * Major entity. @author MyEclipse Persistence Tools
 */

public class Major implements java.io.Serializable {

	// Fields

	private Integer majId;
	private Job job;
	private String majName;
	private String majType;
	private String majSub;
	private String majCat;
	private String majPre;
	private String majInt;
	private String majEmp;
	private Integer majPra;
	private Integer majExp;
	private Integer majWri;
	private Integer majGen;
	private Set majorScores = new HashSet(0);
	private Set universityMajors = new HashSet(0);

	// Constructors

	/** default constructor */
	public Major() {
	}

	/** minimal constructor */
	public Major(Integer majId, Job job, String majName, String majType,
			String majSub, String majCat, String majPre, String majInt,
			String majEmp, Integer majPra, Integer majExp, Integer majWri,
			Integer majGen) {
		this.majId = majId;
		this.job = job;
		this.majName = majName;
		this.majType = majType;
		this.majSub = majSub;
		this.majCat = majCat;
		this.majPre = majPre;
		this.majInt = majInt;
		this.majEmp = majEmp;
		this.majPra = majPra;
		this.majExp = majExp;
		this.majWri = majWri;
		this.majGen = majGen;
	}

	/** full constructor */
	public Major(Integer majId, Job job, String majName, String majType,
			String majSub, String majCat, String majPre, String majInt,
			String majEmp, Integer majPra, Integer majExp, Integer majWri,
			Integer majGen, Set majorScores, Set universityMajors) {
		this.majId = majId;
		this.job = job;
		this.majName = majName;
		this.majType = majType;
		this.majSub = majSub;
		this.majCat = majCat;
		this.majPre = majPre;
		this.majInt = majInt;
		this.majEmp = majEmp;
		this.majPra = majPra;
		this.majExp = majExp;
		this.majWri = majWri;
		this.majGen = majGen;
		this.majorScores = majorScores;
		this.universityMajors = universityMajors;
	}

	// Property accessors

	public Integer getMajId() {
		return this.majId;
	}

	public void setMajId(Integer majId) {
		this.majId = majId;
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getMajName() {
		return this.majName;
	}

	public void setMajName(String majName) {
		this.majName = majName;
	}

	public String getMajType() {
		return this.majType;
	}

	public void setMajType(String majType) {
		this.majType = majType;
	}

	public String getMajSub() {
		return this.majSub;
	}

	public void setMajSub(String majSub) {
		this.majSub = majSub;
	}

	public String getMajCat() {
		return this.majCat;
	}

	public void setMajCat(String majCat) {
		this.majCat = majCat;
	}

	public String getMajPre() {
		return this.majPre;
	}

	public void setMajPre(String majPre) {
		this.majPre = majPre;
	}

	public String getMajInt() {
		return this.majInt;
	}

	public void setMajInt(String majInt) {
		this.majInt = majInt;
	}

	public String getMajEmp() {
		return this.majEmp;
	}

	public void setMajEmp(String majEmp) {
		this.majEmp = majEmp;
	}

	public Integer getMajPra() {
		return this.majPra;
	}

	public void setMajPra(Integer majPra) {
		this.majPra = majPra;
	}

	public Integer getMajExp() {
		return this.majExp;
	}

	public void setMajExp(Integer majExp) {
		this.majExp = majExp;
	}

	public Integer getMajWri() {
		return this.majWri;
	}

	public void setMajWri(Integer majWri) {
		this.majWri = majWri;
	}

	public Integer getMajGen() {
		return this.majGen;
	}

	public void setMajGen(Integer majGen) {
		this.majGen = majGen;
	}

	public Set getMajorScores() {
		return this.majorScores;
	}

	public void setMajorScores(Set majorScores) {
		this.majorScores = majorScores;
	}

	public Set getUniversityMajors() {
		return this.universityMajors;
	}

	public void setUniversityMajors(Set universityMajors) {
		this.universityMajors = universityMajors;
	}

}