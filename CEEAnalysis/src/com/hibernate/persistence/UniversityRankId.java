package com.hibernate.persistence;

/**
 * UniversityRankId entity. @author MyEclipse Persistence Tools
 */

public class UniversityRankId implements java.io.Serializable {

	// Fields

	private University university;
	private String date;

	// Constructors

	/** default constructor */
	public UniversityRankId() {
	}

	/** full constructor */
	public UniversityRankId(University university, String date) {
		this.university = university;
		this.date = date;
	}

	// Property accessors

	public University getUniversity() {
		return this.university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UniversityRankId))
			return false;
		UniversityRankId castOther = (UniversityRankId) other;

		return ((this.getUniversity() == castOther.getUniversity()) || (this
				.getUniversity() != null && castOther.getUniversity() != null && this
				.getUniversity().equals(castOther.getUniversity())))
				&& ((this.getDate() == castOther.getDate()) || (this.getDate() != null
						&& castOther.getDate() != null && this.getDate()
						.equals(castOther.getDate())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getUniversity() == null ? 0 : this.getUniversity()
						.hashCode());
		result = 37 * result
				+ (getDate() == null ? 0 : this.getDate().hashCode());
		return result;
	}

}