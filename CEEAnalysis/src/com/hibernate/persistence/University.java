package com.hibernate.persistence;

import java.util.HashSet;
import java.util.Set;

/**
 * University entity. @author MyEclipse Persistence Tools
 */

public class University implements java.io.Serializable {

	// Fields

	private Integer uniId;
	private City city;
	private Province province;
	private String uniName;
	private String uniType;
	private String uniPro;
	private String uniInt;
	private Set universityRanks = new HashSet(0);
	private Set universityMajors = new HashSet(0);
	private Set recruits = new HashSet(0);
	private Set majorScores = new HashSet(0);

	// Constructors

	/** default constructor */
	public University() {
	}

	/** minimal constructor */
	public University(Integer uniId, City city, Province province,
			String uniName, String uniType, String uniPro, String uniInt) {
		this.uniId = uniId;
		this.city = city;
		this.province = province;
		this.uniName = uniName;
		this.uniType = uniType;
		this.uniPro = uniPro;
		this.uniInt = uniInt;
	}

	/** full constructor */
	public University(Integer uniId, City city, Province province,
			String uniName, String uniType, String uniPro, String uniInt,
			Set universityRanks, Set universityMajors, Set recruits,
			Set majorScores) {
		this.uniId = uniId;
		this.city = city;
		this.province = province;
		this.uniName = uniName;
		this.uniType = uniType;
		this.uniPro = uniPro;
		this.uniInt = uniInt;
		this.universityRanks = universityRanks;
		this.universityMajors = universityMajors;
		this.recruits = recruits;
		this.majorScores = majorScores;
	}

	// Property accessors

	public Integer getUniId() {
		return this.uniId;
	}

	public void setUniId(Integer uniId) {
		this.uniId = uniId;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getUniName() {
		return this.uniName;
	}

	public void setUniName(String uniName) {
		this.uniName = uniName;
	}

	public String getUniType() {
		return this.uniType;
	}

	public void setUniType(String uniType) {
		this.uniType = uniType;
	}

	public String getUniPro() {
		return this.uniPro;
	}

	public void setUniPro(String uniPro) {
		this.uniPro = uniPro;
	}

	public String getUniInt() {
		return this.uniInt;
	}

	public void setUniInt(String uniInt) {
		this.uniInt = uniInt;
	}

	public Set getUniversityRanks() {
		return this.universityRanks;
	}

	public void setUniversityRanks(Set universityRanks) {
		this.universityRanks = universityRanks;
	}

	public Set getUniversityMajors() {
		return this.universityMajors;
	}

	public void setUniversityMajors(Set universityMajors) {
		this.universityMajors = universityMajors;
	}

	public Set getRecruits() {
		return this.recruits;
	}

	public void setRecruits(Set recruits) {
		this.recruits = recruits;
	}

	public Set getMajorScores() {
		return this.majorScores;
	}

	public void setMajorScores(Set majorScores) {
		this.majorScores = majorScores;
	}

}