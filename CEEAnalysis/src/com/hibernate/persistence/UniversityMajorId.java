package com.hibernate.persistence;

/**
 * UniversityMajorId entity. @author MyEclipse Persistence Tools
 */

public class UniversityMajorId implements java.io.Serializable {

	// Fields

	private University university;
	private Major major;

	// Constructors

	/** default constructor */
	public UniversityMajorId() {
	}

	/** full constructor */
	public UniversityMajorId(University university, Major major) {
		this.university = university;
		this.major = major;
	}

	// Property accessors

	public University getUniversity() {
		return this.university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public Major getMajor() {
		return this.major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UniversityMajorId))
			return false;
		UniversityMajorId castOther = (UniversityMajorId) other;

		return ((this.getUniversity() == castOther.getUniversity()) || (this
				.getUniversity() != null && castOther.getUniversity() != null && this
				.getUniversity().equals(castOther.getUniversity())))
				&& ((this.getMajor() == castOther.getMajor()) || (this
						.getMajor() != null && castOther.getMajor() != null && this
						.getMajor().equals(castOther.getMajor())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getUniversity() == null ? 0 : this.getUniversity()
						.hashCode());
		result = 37 * result
				+ (getMajor() == null ? 0 : this.getMajor().hashCode());
		return result;
	}

}