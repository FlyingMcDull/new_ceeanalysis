package com.hibernate.persistence;

/**
 * Scores entity. @author MyEclipse Persistence Tools
 */

public class Scores implements java.io.Serializable {

	// Fields

	private ScoresId id;
	private Integer fs;
	private Integer ss;
	private Integer ts;

	// Constructors

	/** default constructor */
	public Scores() {
	}

	/** full constructor */
	public Scores(ScoresId id, Integer fs, Integer ss, Integer ts) {
		this.id = id;
		this.fs = fs;
		this.ss = ss;
		this.ts = ts;
	}

	// Property accessors

	public ScoresId getId() {
		return this.id;
	}

	public void setId(ScoresId id) {
		this.id = id;
	}

	public Integer getFs() {
		return this.fs;
	}

	public void setFs(Integer fs) {
		this.fs = fs;
	}

	public Integer getSs() {
		return this.ss;
	}

	public void setSs(Integer ss) {
		this.ss = ss;
	}

	public Integer getTs() {
		return this.ts;
	}

	public void setTs(Integer ts) {
		this.ts = ts;
	}

}