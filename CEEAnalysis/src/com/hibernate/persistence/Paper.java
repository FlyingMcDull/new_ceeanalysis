package com.hibernate.persistence;

/**
 * Paper entity. @author MyEclipse Persistence Tools
 */

public class Paper implements java.io.Serializable {

	// Fields

	private Integer pid;
	private String date;
	private Integer proId;
	private String psub;

	// Constructors

	/** default constructor */
	public Paper() {
	}

	/** full constructor */
	public Paper(Integer pid, String date, Integer proId, String psub) {
		this.pid = pid;
		this.date = date;
		this.proId = proId;
		this.psub = psub;
	}

	// Property accessors

	public Integer getPid() {
		return this.pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getProId() {
		return this.proId;
	}

	public void setProId(Integer proId) {
		this.proId = proId;
	}

	public String getPsub() {
		return this.psub;
	}

	public void setPsub(String psub) {
		this.psub = psub;
	}

}