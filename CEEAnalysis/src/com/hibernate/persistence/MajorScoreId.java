package com.hibernate.persistence;

/**
 * MajorScoreId entity. @author MyEclipse Persistence Tools
 */

public class MajorScoreId implements java.io.Serializable {

	// Fields

	private Major major;
	private University university;
	private String date;
	private Province province;

	// Constructors

	/** default constructor */
	public MajorScoreId() {
	}

	/** full constructor */
	public MajorScoreId(Major major, University university, String date,
			Province province) {
		this.major = major;
		this.university = university;
		this.date = date;
		this.province = province;
	}

	// Property accessors

	public Major getMajor() {
		return this.major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}

	public University getUniversity() {
		return this.university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof MajorScoreId))
			return false;
		MajorScoreId castOther = (MajorScoreId) other;

		return ((this.getMajor() == castOther.getMajor()) || (this.getMajor() != null
				&& castOther.getMajor() != null && this.getMajor().equals(
				castOther.getMajor())))
				&& ((this.getUniversity() == castOther.getUniversity()) || (this
						.getUniversity() != null
						&& castOther.getUniversity() != null && this
						.getUniversity().equals(castOther.getUniversity())))
				&& ((this.getDate() == castOther.getDate()) || (this.getDate() != null
						&& castOther.getDate() != null && this.getDate()
						.equals(castOther.getDate())))
				&& ((this.getProvince() == castOther.getProvince()) || (this
						.getProvince() != null
						&& castOther.getProvince() != null && this
						.getProvince().equals(castOther.getProvince())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getMajor() == null ? 0 : this.getMajor().hashCode());
		result = 37
				* result
				+ (getUniversity() == null ? 0 : this.getUniversity()
						.hashCode());
		result = 37 * result
				+ (getDate() == null ? 0 : this.getDate().hashCode());
		result = 37 * result
				+ (getProvince() == null ? 0 : this.getProvince().hashCode());
		return result;
	}

}