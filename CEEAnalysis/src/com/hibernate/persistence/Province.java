package com.hibernate.persistence;

import java.util.HashSet;
import java.util.Set;

/**
 * Province entity. @author MyEclipse Persistence Tools
 */

public class Province implements java.io.Serializable {

	// Fields

	private Integer proId;
	private String proName;
	private String proArea;
	private Set scoreses = new HashSet(0);
	private Set recruits = new HashSet(0);
	private Set universities = new HashSet(0);
	private Set userInfs = new HashSet(0);
	private Set majorScores = new HashSet(0);
	private Set cities = new HashSet(0);

	// Constructors

	/** default constructor */
	public Province() {
	}

	/** minimal constructor */
	public Province(Integer proId, String proName, String proArea) {
		this.proId = proId;
		this.proName = proName;
		this.proArea = proArea;
	}

	/** full constructor */
	public Province(Integer proId, String proName, String proArea,
			Set scoreses, Set recruits, Set universities, Set userInfs,
			Set majorScores, Set cities) {
		this.proId = proId;
		this.proName = proName;
		this.proArea = proArea;
		this.scoreses = scoreses;
		this.recruits = recruits;
		this.universities = universities;
		this.userInfs = userInfs;
		this.majorScores = majorScores;
		this.cities = cities;
	}

	// Property accessors

	public Integer getProId() {
		return this.proId;
	}

	public void setProId(Integer proId) {
		this.proId = proId;
	}

	public String getProName() {
		return this.proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getProArea() {
		return this.proArea;
	}

	public void setProArea(String proArea) {
		this.proArea = proArea;
	}

	public Set getScoreses() {
		return this.scoreses;
	}

	public void setScoreses(Set scoreses) {
		this.scoreses = scoreses;
	}

	public Set getRecruits() {
		return this.recruits;
	}

	public void setRecruits(Set recruits) {
		this.recruits = recruits;
	}

	public Set getUniversities() {
		return this.universities;
	}

	public void setUniversities(Set universities) {
		this.universities = universities;
	}

	public Set getUserInfs() {
		return this.userInfs;
	}

	public void setUserInfs(Set userInfs) {
		this.userInfs = userInfs;
	}

	public Set getMajorScores() {
		return this.majorScores;
	}

	public void setMajorScores(Set majorScores) {
		this.majorScores = majorScores;
	}

	public Set getCities() {
		return this.cities;
	}

	public void setCities(Set cities) {
		this.cities = cities;
	}

}