package com.hibernate.persistence;

/**
 * Recruit entity. @author MyEclipse Persistence Tools
 */

public class Recruit implements java.io.Serializable {

	// Fields

	private RecruitId id;
	private Integer recSco;
	private Integer recNum;

	// Constructors

	/** default constructor */
	public Recruit() {
	}

	/** full constructor */
	public Recruit(RecruitId id, Integer recSco, Integer recNum) {
		this.id = id;
		this.recSco = recSco;
		this.recNum = recNum;
	}

	// Property accessors

	public RecruitId getId() {
		return this.id;
	}

	public void setId(RecruitId id) {
		this.id = id;
	}

	public Integer getRecSco() {
		return this.recSco;
	}

	public void setRecSco(Integer recSco) {
		this.recSco = recSco;
	}

	public Integer getRecNum() {
		return this.recNum;
	}

	public void setRecNum(Integer recNum) {
		this.recNum = recNum;
	}

}