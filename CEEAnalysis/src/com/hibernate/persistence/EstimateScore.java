package com.hibernate.persistence;

/**
 * EstimateScore entity. @author MyEclipse Persistence Tools
 */

public class EstimateScore implements java.io.Serializable {

	// Fields

	private Integer uid;
	private UserInf userInf;
	private Integer chi;
	private Integer math;
	private Integer eng;
	private Integer com;

	// Constructors

	/** default constructor */
	public EstimateScore() {
	}

	/** full constructor */
	public EstimateScore(Integer uid, UserInf userInf, Integer chi,
			Integer math, Integer eng, Integer com) {
		this.uid = uid;
		this.userInf = userInf;
		this.chi = chi;
		this.math = math;
		this.eng = eng;
		this.com = com;
	}

	// Property accessors

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public UserInf getUserInf() {
		return this.userInf;
	}

	public void setUserInf(UserInf userInf) {
		this.userInf = userInf;
	}

	public Integer getChi() {
		return this.chi;
	}

	public void setChi(Integer chi) {
		this.chi = chi;
	}

	public Integer getMath() {
		return this.math;
	}

	public void setMath(Integer math) {
		this.math = math;
	}

	public Integer getEng() {
		return this.eng;
	}

	public void setEng(Integer eng) {
		this.eng = eng;
	}

	public Integer getCom() {
		return this.com;
	}

	public void setCom(Integer com) {
		this.com = com;
	}

}