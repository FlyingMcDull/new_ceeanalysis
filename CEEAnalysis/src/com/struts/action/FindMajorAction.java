package com.struts.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.hibernate.persistence.Major;
import com.hibernate.util.HibernateUtil;
import com.struts.form.MajorForm;

//专业评测
public class FindMajorAction extends DispatchAction {
	
	private String result;
	
	public ActionForward findMajor(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IllegalStateException, SystemException, IOException {
        // request.setCharacterEncoding("utf-8");
	    //System.out.println(request.getParameter("subject"));
		MajorForm majorForm = (MajorForm)form;
		String majType = request.getParameter("subject");
        //majType = new String(majType.getBytes("ISO-8859-1"), "UTF-8");
		String[] majSub = request.getParameter("area").split(",");
		String[] jname = request.getParameter("work").split(",");
		int majPra = Integer.parseInt(request.getParameter("hand").toString());
		int majExp = Integer.parseInt(request.getParameter("talk").toString());
		int majWri = Integer.parseInt(request.getParameter("write").toString());
		int majGen = Integer.parseInt(request.getParameter("sex").toString());

//		byte[] bytes1 = majType.getBytes("UTF-8");
//		String newmajType = new String(bytes1,"UTF-8");
//		System.out.println(newmajType);
//		for (int i = 0; i < majSub.length; i++){
//			byte[] bytes2 = majSub[i].getBytes("UTF-8");
//			String newmajSub = new String(bytes2,"UTF-8");
//			majSub[i] = newmajSub;
//		}
//		for (int i = 0; i < jname.length; i++){
//			byte[] bytes3 = jname[i].getBytes("UTF-8");
//			String newjname = new String(bytes3,"UTF-8");
//			jname[i] = newjname;
//		}
//		String majType = "鐞�;
//		String[]  majSub = {"宸ヤ笟","閲戣瀺"};
//		String[]  jname = {"宸ョ▼甯�,"浼佷笟瀹�};
//		int majPra = 3;
//		int majExp = 3;
//		int majWri = 3;
//		int majGen = 3;
//		System.out.println(majType);		
		List<Major> majors = HibernateUtil.FindMajor(majType, majSub, jname, majPra, majExp, majWri, majGen);
	//	request.setAttribute("majors", majors);
		System.out.println(majors.size());

//		JSONObject json1 = JSONObject.fromObject(majors);
//		JsonConfig jsonConfig = new JsonConfig();
//		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
//		JSONArray json = JSONArray.fromObject(majors, jsonConfig); 
//		result = json.toString();
//		System.out.println(result);
//		if(result != null && !majors.isEmpty()){
//			return new ActionForward("/analysis.jsp");
//		}
//		else{
//			return new ActionForward("/unsuccess.jsp");
//		}
	
//		StringBuffer json=new StringBuffer("[");
//		
//		for(int i =0;i<majors.size();i++)
//		{
//			Major major =majors.get(i);
//			json.append("{");
//			json.append("\"majName\":\""+major.getMajName()+"\",");
//			json.append("\"majPre\":\""+major.getMajPre()+"\",");
//			json.append("\"majInt\":\""+major.getMajInt()+"\",");
//			json.append("\"majEmp\":\""+major.getMajEmp()+"\"");
//			json.append("}");
//		     
//		}
//		    json.append("]");
//		System.out.println(json.toString());
//		response.getWriter().print(json.toString());
	    return null;
	    
	}
}
