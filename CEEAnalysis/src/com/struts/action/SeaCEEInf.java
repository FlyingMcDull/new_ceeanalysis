package com.struts.action;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.hibernate.util.HibernateUtil;

public class SeaCEEInf extends DispatchAction{
	
	//二维数组表示四个省（只取四个）的一二三本比例
	static final double[][] accRate = {
		{0.25, 0.79, 0.9},
		{0.22, 0.92, 0.95},
		{0.11, 0.43, 0.8},
		{0.07, 0.35, 0.8}
	};
	
	//分数线预测
	public ActionForward preGrade(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		int firstBatch, secondBatch, thirdBatch;	
		
		request.getSession().setAttribute("provinceid", 2);
		request.getSession().setAttribute("usertype", "li");
		request.getSession().setAttribute("userid", 14);
		
		String pro = request.getSession().getAttribute("provinceid").toString();
		String type = request.getSession().getAttribute("usertype").toString();
		int proId = Integer.parseInt(pro);
		
//		System.out.println("proId:" + proId);
//		System.out.println("type:" + type);
		
		List scores = HibernateUtil.getScores(proId, type);
		if(scores != null){
			int size = scores.size();
			
			try {
				Collections.sort(scores);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			int firRank = (int)(size*accRate[proId][2]);
			int secRank = (int)(size*accRate[proId][1]);
			int thiRank = (int)(size*accRate[proId][0]);
			
			
			
			firstBatch = Integer.parseInt(String.valueOf(scores.get(firRank)));
			secondBatch = Integer.parseInt(String.valueOf(scores.get(secRank)));
			thirdBatch = Integer.parseInt(String.valueOf(scores.get(thiRank)));
			
			System.out.println("firRank:" + firstBatch);
			System.out.println("secRand:" + secondBatch);
			System.out.println("thiRand:" + thirdBatch);
			
			request.getSession().setAttribute("firstBatch", firstBatch);
			request.getSession().setAttribute("secondBatch", secondBatch);
			request.getSession().setAttribute("thirdBatch", thirdBatch);
			return  new ActionForward("/score.jsp");	
		}
		else {
			return new ActionForward("/unsuccess.jsp");
		}
	}
}
