package com.struts.action;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.hibernate.persistence.University;
import com.hibernate.util.HibernateUtil;
import com.struts.form.UniversityForm;

public class FindUniversityAction extends DispatchAction{
	public ActionForward findUni(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IllegalStateException, SystemException, IOException{
		UniversityForm universityForm = (UniversityForm)form;
//		String uniName = universityForm.getUniName();
		String proName = universityForm.getProName();
		int uScore = universityForm.getuScore();
		List<University> universitys = HibernateUtil.FindUniversity(proName,uScore);
//		request.setAttribute("universitys", universitys);
//		JSONObject json = new JSONObject();
//		json.put("universitys", universitys);
//		response.getWriter().print(json.toString());
//		if(universitys!=null && !universitys.isEmpty()){
//			return mapping.findForward("findUniSuc");
//		}else {
//			return mapping.findForward("findUniFai");
//		}
		return new ActionForward("/Recommendation.html");
	}
}
