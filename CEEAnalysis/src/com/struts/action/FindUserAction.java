package com.struts.action;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.hibernate.persistence.Province;
import com.hibernate.persistence.UserInf;
import com.hibernate.util.HibernateUtil;

//�û���¼
public class FindUserAction extends DispatchAction{
	public ActionForward findUser(ActionMapping mapping, ActionForm form, HttpServletRequest request,
									HttpServletResponse response) throws IllegalStateException, SystemException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		List list = HibernateUtil.FindUser(username, password);
		UserInf user = new UserInf();
		if(list!=null && !list.isEmpty()){
			for(int i=0; i<list.size(); ++i){
				user = (UserInf)list.get(i);
				request.getSession().setAttribute("userid", user.getUid());
				request.getSession().setAttribute("username", user.getUname());
				request.getSession().setAttribute("password", user.getUpw());
				request.getSession().setAttribute("useremail", user.getUemail());
				request.getSession().setAttribute("usertype", user.getUtype());
				request.getSession().setAttribute("userscore", user.getUscore());
				request.getSession().setAttribute("provinceid", user.getProvince().getProId());
			}
			return new ActionForward("/home_page.jsp");
		}else {
			return new ActionForward("/login.html");
		}
	}
	
	//�û�ע��
	public ActionForward register(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String type = request.getParameter("type");
		Integer province_id = Integer.parseInt(request.getParameter("province_id"));
		List list = HibernateUtil.findPro(province_id);
		Province province = (Province)list.get(0);
		boolean flag = HibernateUtil.register(username, password, email, type, province);
		UserInf user = new UserInf();
		user.setUname(username);
		request.getSession().setAttribute("username", user.getUname());
		if(flag){
			return new ActionForward("/home_page.jsp");
		}else {
			return new ActionForward("/unsuccess.jsp");
		}
	}
	
	//�һ�����
	public ActionForward findPw(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IllegalStateException, SystemException {
//		UserForm userForm = (UserForm)form;
//		String username = userForm.getUsername();
		String username = request.getParameter("username");
		UserInf user = new UserInf();
		List users = HibernateUtil.FindUser(username);
		if(users!=null && !users.isEmpty()){
				user = (UserInf)users.get(0);
//				String pw = user.getUpw();
//				String email = user.getUemail();
//				request.getSession().setAttribute("pw", pw);
//				request.getSession().setAttribute("email", email);
				try{
					Properties prop = new Properties();
					prop.setProperty("mail.transport.protocol", "smtp");
					prop.setProperty("mail.smtp.host", "smtp.sina.com");
					prop.setProperty("mail.smtp.auth", "true");
					prop.setProperty("mail.debug", "true");
					Session session = Session.getInstance(prop);
					
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress("top9413@sina.com"));
					msg.setRecipient(RecipientType.TO, new InternetAddress(user.getUemail()));
					msg.setSubject(user.getUname()+"����");
					msg.setText("��������"+user.getUpw());
				
					Transport trans = session.getTransport();
					trans.connect("top9413", "906344");
					trans.sendMessage(msg, msg.getAllRecipients());
				}catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				return new ActionForward("/login.html");
		}else {
			return new ActionForward("/unsuccess.jsp");
		}
	}
	
	//�޸�����
	public ActionForward modPass(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
        //request.getSession().setAttribute("userid", 14);
		String userid = request.getSession().getAttribute("userid").toString();
		int userId = Integer.parseInt(userid);
		String password = request.getParameter("password");
		boolean flag = HibernateUtil.modPass(userId, password);
		if(flag){
			System.out.println("userId:"+userId);
			System.out.println("password:"+password);
			return new ActionForward("/manage.jsp");
		}else {
			return new ActionForward("/unsuccess.jsp");
		}
	}
	
	//�޸�����
	public ActionForward modEmail(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
        //request.getSession().setAttribute("userid", 14);
		
		String userid = request.getSession().getAttribute("userid").toString();
		int userId = Integer.parseInt(userid);
		String email = request.getParameter("email");
		boolean flag = HibernateUtil.modEmail(userId, email);
		if(flag){
			System.out.println("userId:"+userId);
			System.out.println("email:"+email);
			return new ActionForward("/manage.jsp");
		}else {
			return new ActionForward("/unsuccess.jsp");
		}
	}
}
