package com.struts.form;

import org.apache.struts.action.ActionForm;

public class UniversityForm extends ActionForm{
	private static final long serialVersionUID = 1141456844854939928L;
	private String uniName;
	private String proName;
	private int uScore;
	public String getUniName() {
		return uniName;
	}
	public void setUniName(String uniName) {
		this.uniName = uniName;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public int getuScore() {
		return uScore;
	}
	public void setuScore(int uScore) {
		this.uScore = uScore;
	}
}
