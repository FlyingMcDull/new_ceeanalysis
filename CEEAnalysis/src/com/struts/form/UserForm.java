package com.struts.form;
import org.apache.struts.action.ActionForm;

public class UserForm extends ActionForm{
	private static final long serialVersionUID = 1141456844854939928L;
	private String username;
	private String password;
	private String uemail;
	
	public String getUemail() {
		return uemail;
	}
	public void setUemail(String uemail) {
		this.uemail = uemail;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
