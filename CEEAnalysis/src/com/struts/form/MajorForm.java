package com.struts.form;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class MajorForm extends ActionForm{
	private static final long serialVersionUID = 1141456844854939928L;
	private String majType;
	private String[] majSub;
	private String[] jname;
	private int majPra;
	private int majExp;
	private int majWri;
	private int majGen;
	public String getMajType() {
		return majType;
	}
	public void setMajType(String majType) throws UnsupportedEncodingException {
		//System.out.println(majType);
		/*
		List<String> list = new ArrayList<String>(100);
		
		for (Byte bit : majType.getBytes("UNICODE")) {
		//	System.out.println(bit & 0xff);
		}
		Iterator<String> itr = Charset.availableCharsets().keySet().iterator();
		while (itr.hasNext()) {
			list.add(itr.next());
		}
		for (String s1 : list) {
			for (String s2 : list) {
				try {
					String s = new String(majType.getBytes(s1), s2);
					if (s.contentEquals("理"))
					System.out.println(s1 + "  " + s2);
				} catch (Exception e){
					
				}
			}
		}
		String s = new String(majType.getBytes("UTF-16"), "UTF-8");
		*/
		
		this.majType = majType;
	}
	public String[] getMajSub() {
		return majSub;
	}
	public void setMajSub(String[] majSub) {
		this.majSub = majSub;
	}
	public String[] getJname() {
		return jname;
	}
	public void setJname(String[] jname) {
		this.jname = jname;
	}
	public int getMajPra() {
		return majPra;
	}
	public void setMajPra(int majPra) {
		this.majPra = majPra;
	}
	public int getMajExp() {
		return majExp;
	}
	public void setMajExp(int majExp) {
		this.majExp = majExp;
	}
	public int getMajWri() {
		return majWri;
	}
	public void setMajWri(int majWri) {
		this.majWri = majWri;
	}
	public int getMajGen() {
		return majGen;
	}
	public void setMajGen(int majGen) {
		this.majGen = majGen;
	}
	
	
}
